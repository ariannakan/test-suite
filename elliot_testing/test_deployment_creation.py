# IMPORT REQUESTS
import requests
import json
import pprint
import re
import random
import urllib3
pp = pprint.PrettyPrinter(indent=1)

urllib3.disable_warnings()

s = requests.Session()
# DEFINE LOGIN CREDENTIALS
# base_url = 'http://127.0.0.1:8000'
base_url = 'https://ksoc-poc-02.knightscope.internal'
uname = 'admin'
pword='test123!!'
# s.verify = False
# LOGIN, SAVE TOKEN
data = {
    'uname': uname,
    'pword':pword
}
response = s.get(base_url + '/login', verify=False)

s.headers.update({'X-CSRFToken':response.cookies['csrftoken'], "Referer": base_url+'/login'})

response = s.post(base_url+'/login', data=data, verify=False)

rand_num = random.randint(0,10000)
print('rand num is', rand_num)
def create_test_deployment():
    data = {
        'post': 'yes',
        'is_active': 'on',
        'deployment_name': f'test_deployment{rand_num}',
        'deployment_address': '6312 Rainbow Drive',
        'start_date_0': '2021-02-01',
        'start_date_1': '22:46:52',
        'end_date_0': '2021-02-01',
        'end_date_1': '22:46:54',
        'KSOC_url': f'test_deployment{rand_num}',
        'ship_date_0': '2021-02-01',
        'ship_date_1': '22:47:01',
        'mq_vhost': 'myvhost',
        'default_lat': '0.0',
        'default_lng': '0.0',
        'deployment_timezone': 'PST',
        'default_map_zoom': 19,
        'default_map_view': 'SATELLITE ',
        'has_alpr': 'on',
        'ebt_units': 'F',
        'ebt_fever_temperature_fahrenheit': 100.4,
        'ebt_alert_fever_only': 'on',
        'has_parking_meter': 'on',
        'has_parking_utilization': 'on',
        'connectivity_type': 'WF',
        'video_storage_in_days': 15,
        'video_cellular_timeout_sec': 300,
        'data_cap': 95129,
        'data_cycle_start_date': 25,
        'calculateParkingUtilization': 'on',
        'alpr_confidence_threshold': 68,
        'alpr_timefilter_threshold': 300,
        'face_similarity_threshold': 80,
        'service_branch_number': '',
        'service_branch_name': '',
        'service_branch_manager': '',
        'service_primary_technician_info': '',
        'service_secondary_technician_info': '',
        'primary_shipping_location': 'branch',
        'branch_shipping_address': '',
        'courier_location_address': '',
        'storage_location': '',
        'service_location': '',
        'person_of_contact': '',
        'person_of_contact_24_7': '',
        'deployment_class': 'aus',
        'deployment_type': 'none'
    }

    response = s.post(base_url + '/admin/main/deploymentdata/add/', data=data, verify=False)
    print(f'status code {response.status_code}')
    matches = [int(num) for num in re.findall(r'deploymentdata/(.*)/change', response.text)]
    matches.sort()
    print(matches)
    new_deployment_id = matches.pop()
    print(f'successfully created new deployment with id {new_deployment_id}')
    return new_deployment_id


def delete_test_deployment(deployment_ID):
    print(f'deleting deployment with deployment_iD={deployment_ID}')
    data = {
        'post': 'yes',
    }
    return s.post(base_url + f'/admin/main/deploymentdata/{deployment_ID}/delete/', data=data, verify=False)



# Create deployment with settings, create_test_deployment() returns a deployment_ID so you can delete it later:
new_deployment_id = create_test_deployment()

# delete deployment, requires deployment_ID from create_test_deployment()
# delete_test_deployment(22)

# you can use this url to verify the deployment was created successfully.
response = s.get(base_url + '/ajax/api/deployments/22/', verify=False)

