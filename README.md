# KSOCV4 test-suite
QA script for testing [ksocv4](https://bitbucket.org/elliotyoung/ksocv4/src/master/) endpoints. This project uses python 3.7
### Installation and Usage
Create 'venv' folder in root directory
```sh
$ python3 -m venv venv
```
Activate venv and install dependencies
```sh
$ source venv/bin/activate
$ pip3 install -r requirements.txt
```
Run test suite on poc-02
```sh
$ python3 testsuite.py -e poc-02 -app API
```



# Notes
When testing 'create user', you must change the username and email each time. Currently, I am manually incrementing the username/email until I find a solution.
