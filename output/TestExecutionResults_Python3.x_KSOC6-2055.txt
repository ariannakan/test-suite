startTime: 20200925_230040

######### Login Controller ##########

Login Test
GET request
url: https://ksoc-poc-02.knightscope.internal/login/
GET Result|200
None
POST request
sURL: https://ksoc-poc-02.knightscope.internal/login/
{'uname': 'ask', 'csrfmiddlewaretoken': None, 'pword': 'knight123'}
request completed
POST Result|200

######### Sector Controller ##########

Sectors for deployment 1
GET request
url: https://ksoc-poc-02.knightscope.internal/deployments/1/sectors/
200|0.422999858856|[{"sector_ID": 1, "deployment_zone_ID": 1, "sector_name": "Front building", "deployment": "1", "information": "zone 1", "levels": ["1", "2"]}]

Details for sector 1 in deployment 1
GET request
url: https://ksoc-poc-02.knightscope.internal/deployments/1/sectors/1
200|0.570000171661|{"sector_ID": 1, "sector_name": "Front building", "deployment_zone_ID": 1, "deployment": "1", "machine_data": [{"MIN": 12}, {"MIN": 15}, {"MIN": 22}, {"MIN": 25}, {"MIN": 26}, {"MIN": 30}, {"MIN": 33}, {"MIN": 58}, {"MIN": 84}], "information": "zone 1", "levels": ["1", "2"], "thermal_day_threshold": null, "thermal_night_threshold": null, "patrol_sound_volume": 36, "intercom_concierge_volume": 36, "alerts_volume": 36}

Boundaries for sector 1 in deployment 1
GET request
url: https://ksoc-poc-02.knightscope.internal/deployments/1/sectors/1/boundaries/
200|0.378999948502|[{"point_ID": 1, "latitude": 0.0, "longitude": 0.0}, {"point_ID": 2, "latitude": 50.0, "longitude": 50.0}, {"point_ID": 3, "latitude": 100.0, "longitude": 100.0}]

Update boundaries for sector 1 in deployment 1
POST request
sURL: https://ksoc-poc-02.knightscope.internal/deployments/1/sectors/1/boundaries/post
{"boundary": [{"latitude": "37.33", "id": 1, "longitude": "121.88"}, {"latitude": "37.32", "id": 2, "longitude": "121.84"}, {"latitude": "37.31", "id": 3, "longitude": "121.80"}]}
request completed
200|0.503999948502|{"success": true}

Get boundaries for sector 1 in deployment 1
GET request
url: https://ksoc-poc-02.knightscope.internal/deployments/1/sectors/1/boundaries/
200|0.59600019455|[{"point_ID": 1, "latitude": 37.33, "longitude": 121.88}, {"point_ID": 2, "latitude": 37.32, "longitude": 121.84}, {"point_ID": 3, "latitude": 37.31, "longitude": 121.8}]

Update boundaries for sector 1 in deployment 1
POST request
sURL: https://ksoc-poc-02.knightscope.internal/deployments/1/sectors/1/boundaries/post
{"boundary": [{"latitude": "0", "id": 1, "longitude": "0"}, {"latitude": "50", "id": 2, "longitude": "50"}, {"latitude": "100", "id": 3, "longitude": "100"}]}
request completed
200|0.414000034332|{"success": true}

Update volume for sector 1 in deployment 1
PUT request
sURL: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/sectors/1/volume/
{'patrol_sound': '36', 'alerts': '36', 'intercom_concierge': '36'}
request completed
200|4.73600006104|{"sector_ID": "1", "failed_machines": ["15"], "successful_machines": ["22", "26", "84"], "success": true}

######### Event Controller ##########

EventGeneratingSchedule for deployment 1; filtered by aod
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/eventgeneratingschedule/
200|0.692000150681|{"success": true, "event_generating_schedule": [{"sector": 1, "deployment": "", "detection_type": "aod", "day_of_week": "thursday", "start_time": "00:00:00", "end_time": "09:00:00", "location": null}, {"sector": 1, "deployment": "", "detection_type": "aod", "day_of_week": "thursday", "start_time": "13:00:00", "end_time": "22:30:00", "location": null}, {"sector": 1, "deployment": "", "detection_type": "aod", "day_of_week": "monday", "start_time": "00:00:00", "end_time": "01:00:00", "location": null}, {"sector": 1, "deployment": "", "detection_type": "aod", "day_of_week": "sunday", "start_time": "00:00:00", "end_time": "01:00:00", "location": null}]}

Create schedule for deployment 1
POST request
sURL: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/eventgeneratingschedule/
{'detection_type': 'alpr', 'schedule': "[{'start_time': '00:00:00', 'end_time': '01:01:01', 'day_of_week': 'monday', 'sector_ID': '1'}, {'start_time': '07:07:07', 'end_time': '08:08:08', 'day_of_week': 'monday', 'sector_ID': '1'}]", 'day_of_week': 'monday', 'sector_ID': '1'}
request completed
200|0.611000061035|{"success": true, "message": "Created new Event Generating Schedule"}

Delete schedule for deployment 1
POST request
sURL: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/eventgeneratingschedule/delete_schedule/post/
{'detection_type': 'alpr', 'deleted_locations_IDs': '[1,3]', 'day_of_week': 'monday', 'sector_ID': '1'}
request completed
200|0.598999977112|{"deployment": "1", "success": true}

Add Event for deployment 1
POST request
sURL: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/events/
{'time_stamp': '1600341300', 'event_type': 'thermal', 'MIN': '12'}
request completed
201|0.847999811172|{"success": true, "event_ID": 22642}

Get Events for deployment 1 with params: {'detection_type': 'thermal', 'machines': '12'}
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/events/
200|0.906000137329|{"deployment": "1", "is_last": false, "events": [{"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22642, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22641, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22640, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22639, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22638, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22637, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22636, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22635, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22634, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22633, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22632, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22631, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22630, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22629, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22628, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22627, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22626, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22625, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22624, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22623, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22622, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22621, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22620, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22619, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}, {"type": "event", "time_stamp": "2020-09-17 11:15:00", "MIN": 12, "latitude": 37.408795, "longitude": -122.074781, "level": 1, "event_type": "thermal", "event_ID": 22618, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "", "clear": false, "customer_self": true, "detection_type": "Others", "manual_entry": true, "metadata": "", "video_exists": false, "created_by": "ask", "false_positive": null, "call_audio_file": false}]}

Update Events for events 22040
PUT request
sURL: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/events/
{'comment': 'testing update 2', 'clear': 'false', 'event_IDs': '22040', 'clear_time_stamp': '1600341300'}
request completed
200|0.641000032425|{"success": true}

Generate event report between 1579759917000 and 1599779917000
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/events/1579759917000/1599779917000/report/
200|0.859000205994

Event Details for event 22040
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/events/22040/
200|0.734000205994|{"deployment": "1", "event": {"type": "event", "time_stamp": "2019-02-01 21:41:46", "MIN": 12, "latitude": 37.4085946478, "longitude": -122.074688557, "level": 1, "event_type": "Blacklisted license plate - BLACKLT", "event_ID": 22040, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "testing update 2", "clear": false, "customer_self": true, "detection_type": "Plates", "manual_entry": false, "metadata": {"alpr_ID": 49852, "image_url": "/ajax/images/testkhq/alpr/BLACKLT_2019-02-01_13:41:46.402652-08:00.jpg", "reason": "hello", "type": "Plates", "employee_image_url": "", "cam9_image_url": "", "matching_image_url": "", "camera": "12"}, "video_exists": false, "created_by": null, "false_positive": null, "call_audio_file": false}}

Update event 22040
PUT request
sURL: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/events/22040/
{'comment': 'false positive, ignore this', 'clear': 'true', 'event_IDs': '22040', 'clear_time_stamp': '1600341300', 'false_positive': 'true'}
request completed
200|0.658999919891|{"success": true, "event_ID": "22040"}

Event Details for event 22040
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/events/22040/
200|0.664000034332|{"deployment": "1", "event": {"type": "event", "time_stamp": "2019-02-01 21:41:46", "MIN": 12, "latitude": 37.4085946478, "longitude": -122.074688557, "level": 1, "event_type": "Blacklisted license plate - BLACKLT", "event_ID": 22040, "sector_ID": 1, "sector_name": "Front building", "threat_level": 5, "comment": "false positive, ignore this", "clear": true, "customer_self": true, "detection_type": "Plates", "manual_entry": false, "metadata": {"alpr_ID": 49852, "image_url": "/ajax/images/testkhq/alpr/BLACKLT_2019-02-01_13:41:46.402652-08:00.jpg", "reason": "hello", "type": "Plates", "employee_image_url": "", "cam9_image_url": "", "matching_image_url": "", "camera": "12"}, "video_exists": false, "created_by": null, "false_positive": null, "clear_time_stamp": "2020-09-17 11:15:00+00:00", "sign_off_user": "ask", "call_audio_file": false}}

Events aggregate
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/events/aggregate/
200|0.605999946594|{"interval": "hourly", "result": [{"timestamp": 1498478400, "count": 1, "type": "People"}, {"timestamp": 1498536000, "count": 1, "type": "People"}, {"timestamp": 1498539600, "count": 2, "type": "People"}, {"timestamp": 1498543200, "count": 1, "type": "People"}, {"timestamp": 1498546800, "count": 1, "type": "People"}, {"timestamp": 1498550400, "count": 2, "type": "People"}, {"timestamp": 1498554000, "count": 1, "type": "People"}, {"timestamp": 1498561200, "count": 1, "type": "People"}, {"timestamp": 1498564800, "count": 2, "type": "People"}, {"timestamp": 1498622400, "count": 2, "type": "People"}, {"timestamp": 1498651200, "count": 3, "type": "People"}]}

Get location event boundaries
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/locationeventboundary/
200|1.19099998474|{"location_event_boundary": [{"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope entrance", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f63ce58811f4c4c69e14937"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f63e77d811f4c54dece6c3f"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f65219a811f4c54dece6c4d"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f65222d811f4c54e2ce6ca2"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f652374811f4c54dece6c51"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f652653811f4c54e2ce6cb0"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f65269e811f4c54e2ce6cb4"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f652c00811f4c54e2ce6cb8"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f65322d811f4c54e2ce6cbc"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f653a69811f4c54e2ce6cc0"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f653e1a811f4c54e2ce6cc4"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f65464a811f4c7707f2cd01"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f68f47b811f4c1c37bc6a64"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f68f586811f4c1c37bc6a69"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6bbee8811f4c3e178ab4c9"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6bbf46811f4c3e178ab4cb"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6bc091811f4c3e178ab4ce"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6bc9aa811f4c3e178ab4d5"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6bd99f811f4c433feb7b6c"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6be39e811f4c44e8d0d1d5"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6cd059811f4c462e9d18bf"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6cd330811f4c618bcdb4c1"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6cd3d4811f4c618acdb4cb"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6cd431811f4c6189cdb4c1"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6cd450811f4c618acdb4d5"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6cd4cc811f4c618bcdb4c8"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6e2add811f4c777b95fe02"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6e2e5c811f4c777b95fe0a"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6e2e71811f4c777995fe08"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6e2fb6811f4c798312b77a"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6e3090811f4c798212b77f"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6e30af811f4c798312b780"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6e3148811f4c798212b785"}, {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope parking lot", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f6e320f811f4c798312b789"}]}

Create location event boundary
POST request
sURL: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/locationeventboundary/
{'detection_type': 'alpr', 'level': '1', 'location_name': 'knightscope parking lot', 'bounding_box_json': '', 'sector_ID': '1'}
request completed
200|0.733000040054|{"success": true, "message": "Created new location event boundary", "event_location": "5f6e6899811f4c798212b79c"}

Get location event boundary from location_ID 5f63ce58811f4c4c69e14937
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/locationeventboundary/5f63ce58811f4c4c69e14937/
200|0.625999927521|{"location_event_boundary": {"sector": "1 - Knightscope HQ - ZoneID 1", "deployment": null, "level": 1, "detection_type": "alpr", "comment": "knightscope entrance", "metadata": null, "created_by": "ask", "bounding_box_json": "", "location_ID": "5f63ce58811f4c4c69e14937"}}

Update location event boundary from location_ID 5f63ce58811f4c4c69e14937
PUT request
sURL: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/locationeventboundary/5f63ce58811f4c4c69e14937/
{'detection_type': 'alpr', 'location_name': 'knightscope entrance', 'bounding_box_json': ''}
request completed
200|0.611000061035|{"success": true, "message": "Updated location event boundary", "event_location": "5f63ce58811f4c4c69e14937"}

Get all eventGeneratingSchedules matching params: {'detection_type': 'alpr', 'day_of_week': 'tuesday'}
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/events/schedule/
200|0.591000080109|{}

Update all eventGeneratingSchedules: {'detection_type': 'asd', 'replace_all': True, 'start_time': '00:00:00', 'day_of_week': '["tuesday"]', 'end_time': '01:01:01'}
POST request
sURL: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/events/schedule/
{'detection_type': 'asd', 'replace_all': True, 'start_time': '00:00:00', 'day_of_week': '["tuesday"]', 'end_time': '01:01:01'}
request completed
200|0.605999946594|{"success": true}

######### Logout ##########

Logout
GET request
url: https://ksoc-poc-02.knightscope.internal/logout
200|0.738000154495

startTime: 20200926_012008

######### Login Controller ##########

Login Test
GET request
url: https://ksoc-poc-02.knightscope.internal/login/
GET Result|200
None
POST request
sURL: https://ksoc-poc-02.knightscope.internal/login/
{'uname': 'ask', 'csrfmiddlewaretoken': None, 'pword': 'knight123'}
request completed
POST Result|200

Admin_site_urls
GET request
url: https://ksoc-poc-02.knightscope.internal/admin/
200|0.888999938965

Deployment Login: stanford
GET request
url: https://ksoc-poc-02.knightscope.internal/stanford/login
200

Deployment Login: tesla
GET request
url: https://ksoc-poc-02.knightscope.internal/tesla/login
401             <-- Correct Response (tesla deployment is not configured yet)

######### User Controller ##########

Authenticate User
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/user/auth/
200|0.587999820709

User Deployments
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/user/deployments/1/user_deployments/
200|0.618000030518|{"deployments": [{"deployment_name": "Knightscope HQ", "deployment_ID": "1", "KSOC_url": "KHQ"}]}

User Settings and Permissions
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/user/
200|0.693000078201|{"username": "ask", "is_kiadmin": true, "token": "XNG1s6Q-KDeaqLYUrWISX3PfJ4o3TDh66Sp_5Um3", "settings": {"is_kiadmin": true, "terms_and_conditions_accepted": true}, "permissions": {"admin": true, "can_view_list_preferences": true, "can_view_cleared_events": true, "can_clear_events": true, "can_add_events": true, "can_view_live_video": true, "can_view_hd_quality_video": true, "can_view_recorded_video": true, "can_make_video_clip": true, "can_view_video_clip_history": true, "can_view_multiple_camera_feed": true, "can_view_patrol_schedule": true, "can_edit_patrol_schedule": true, "can_patrol_stop_pause_machine": true, "can_make_alarm_sound": true, "can_edit_patrol_sound": true, "can_initiate_live_audio": true, "can_initiate_intercom_call": true, "can_make_pre_recorded_messages": true, "can_make_custom_broadcast": true, "can_view_investigate_page": true, "can_generate_reports": true, "can_view_number_of_detections": true, "can_view_plate_parking_meter_top_ten": true, "can_view_device_top_ten": true, "can_view_device_statistics": true, "can_view_machine_usage": true, "can_view_lte_usage": true}, "email_notification": false, "text_notification": false, "voice_notification": false, "phone_number": null, "email": "ask@knightscope.com", "first_name": "", "last_name": "", "deployment_ID": "1"}

User Exists: ask
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/user/user_exists/ask/
200|0.786999940872|{"exists": true}

User Access MIN: 12
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/k5/12/auth/
200|0.834000110626

Get all users: 1
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/users/
200|1.00300002098|{"deployment": "1", "users": [{"username": "knightscope"}, {"username": "urjapatel"}, {"username": "philiptranbavo@gmail.com"}, {"username": "anyee18331@gmail.com"}, {"username": "ovbtp@gmail.com"}, {"username": "asda@asda.cad"}, {"username": "test@test.test"}, {"username": "pf34@gmail.com"}]}

Create User: Arianna Kan
POST request
sURL: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/users/
{'username': 'akan@wpi.edu', 'first_name': 'Arianna', 'last_name': 'Kan', 'email': 'akan@wpi.edu', 'deployment_list': '1,2'}
request completed
500|0.969000101089|{"error": "Internal server error"}       <-- Correct Response (user akan@wpi.edu is already created)

Update User: akan@wpi.edu
PUT request
sURL: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/users/akan@wpi.edu/
{'is_active': 'True', 'op': 'user_active'}
request completed
200|0.862999916077|{"success": true, "user": "akan@wpi.edu"}

User Details for akan@wpi.edu
GET request
url: https://ksoc-poc-02.knightscope.internal/ajax/deployments/1/users/akan@wpi.edu/
200|0.676000118256|{"deployment": "1", "deployment_list": [], "permissions": {"admin": false, "can_view_list_preferences": true, "can_view_cleared_events": true, "can_clear_events": true, "can_add_events": true, "can_view_live_video": true, "can_view_hd_quality_video": true, "can_view_recorded_video": true, "can_make_video_clip": true, "can_view_video_clip_history": true, "can_view_multiple_camera_feed": true, "can_view_patrol_schedule": true, "can_edit_patrol_schedule": true, "can_patrol_stop_pause_machine": true, "can_make_alarm_sound": true, "can_edit_patrol_sound": true, "can_initiate_live_audio": true, "can_initiate_intercom_call": true, "can_make_pre_recorded_messages": true, "can_make_custom_broadcast": true, "can_view_investigate_page": true, "can_generate_reports": true, "can_view_number_of_detections": true, "can_view_plate_parking_meter_top_ten": true, "can_view_device_top_ten": true, "can_view_device_statistics": true, "can_view_machine_usage": true, "can_view_lte_usage": true}, "username": "akan@wpi.edu", "ksoc_version": "v6"}

######### Deployment Controller ##########

Chargepad Location for deployment 4
GET request
url: https://ksoc-poc-02.knightscope.internal/deployments/4/chargepad/
200|0.515000104904|{"deployment": "4", "charge_pad": []}

Deployment 4 Details
GET request
url: https://ksoc-poc-02.knightscope.internal/deployments/4/
200|2.02200007439|{"deployment_ID": "4", "deployment_name": "Uber", "deployment_address": "130 Vermont St, San Francisco, CA 94103", "is_active": null, "sectors": [{"sector_name": "Zone 2", "sector_ID": 2, "machines": [{"MIN": 68}], "information": "Secondary Location"}, {"sector_name": "Zone 1", "sector_ID": 13, "machines": [{"MIN": 10}, {"MIN": 17}, {"MIN": 18}, {"MIN": 19}, {"MIN": 28}, {"MIN": 29}, {"MIN": 35}, {"MIN": 1100}], "information": "zone 1"}, {"sector_name": "zone 4", "sector_ID": 18, "machines": [{"MIN": 82}], "information": "Zone 4"}, {"sector_name": "zone 3", "sector_ID": 17, "machines": [], "information": "Zone 3"}, {"sector_name": "zone 14", "sector_ID": 19, "machines": [], "information": "Zone 14"}, {"sector_name": "zone 10", "sector_ID": 20, "machines": [], "information": "zone 10"}, {"sector_name": "testEventGenSched", "sector_ID": 21, "machines": [], "information": "hi"}], "mq_url": "wss://ws-elb.ksoc.co/stomp/websocket", "mq_vhost": "uber_vhost", "start_date": "2016-06-27 12:00:00+00:00", "end_date": "None", "ship_date": "2016-06-27 12:16:02+00:00", "KSOC_url": "UBER", "default_video_to_four_cameras": null, "default_lat": 37.325879, "default_lng": -121.94561, "default_map_view": null, "deployment_timezone": "America/Los_Angeles", "default_map_zoom": 18, "has_indoormap": true, "has_aod": true, "has_thermal": true, "has_alpr": true, "has_asd": true, "has_badges": null, "has_control_panel": false, "has_patrol_scheduler": false, "can_park_near_chargepad": false, "has_timeline": false, "has_parking_meter": null, "has_parking_utilization": null, "has_face_recognition": null, "aws_rekognition_collection": null, "connectivity_type": "WF", "cellular_timeout_sec": null, "video_storage_in_days": null, "has_data_overage_charge": null, "sso_provider": null}

Indoormap boundaries for deployment 4
GET request
url: https://ksoc-poc-02.knightscope.internal/deployments/4/indoormap/boundaries/
200|0.523999929428|{"coordinates": [{"lat": 37.32282986, "lng": -121.9486588, "parameter": "ne"}, {"lat": 37.32817633, "lng": -121.94210977, "parameter": "sw"}], "url": "https://ksocwestfieldvf.s3.amazonaws.com/indoormap/"}

Indoormap for deployment 4
GET request
url: https://ksoc-poc-02.knightscope.internal/deployments/4/indoormap/get/
200|0.849999904633|{"url": "http://ksocwestfieldvf.s3.amazonaws.com/indoormap/"}

Levels for deployment 4
GET request
url: https://ksoc-poc-02.knightscope.internal/deployments/4/levels
200|0.257999897003|{"1": "L1", "2": "L2"}

Machines Alive for deployment 4
GET request
url: https://ksoc-poc-02.knightscope.internal/deployments/4/alive
200|0.388000011444|{"machines": [{"MIN": 10, "online": true}, {"MIN": 17, "online": true}, {"MIN": 18, "online": true}, {"MIN": 19, "online": true}, {"MIN": 28, "online": true}, {"MIN": 29, "online": true}, {"MIN": 35, "online": true}, {"MIN": 68, "online": false}, {"MIN": 82, "online": true}, {"MIN": 1100, "online": false}], "deployment_ID": "4"}

######### Logout ##########

Logout
GET request
url: https://ksoc-poc-02.knightscope.internal/logout
200|1.85500001907

