import json
import time
from datetime import datetime
import argparse
import os, sys
import urllib3
import logging

from src.common.runtimeExecutor import runTestSuite
from src.common.globalSettings import generateTestExecution
from src.common.dataCollection import extract_dataFiles
from src.common.resultGenerator import generate_results

urllib3.disable_warnings()

############################
#Get Start DateTime
#############################
startMainTime=time.time()
startTime=datetime.now().strftime('%Y%m%d_%H%M%S')
logging.info("startTime: "+startTime+"\n")
#############################
parser = argparse.ArgumentParser(description='Takes in parameters to run; app for application_type, env for testing_environment')
parser.add_argument('-app', help='application type to test: API or UI', required=True)
parser.add_argument('-e','--env',help='environment to send event', required=True)

parser.add_argument('-comp', help='component: user, deployment, sector.')

parser.add_argument('-report', help='specify what type of report to generate')
args = parser.parse_args()


if ((args.app != "API") and (args.env != "UI")):
	exit("Error:  --app must be API or UI only\n")
if args.app == 'API':
    application_type = 'API'
elif args.app == 'UI':
    application_type = 'UI'
if args.comp is not None:
	component = args.comp
else:
	component = 'all'


if ((args.env != "poc-01") and (args.env != "poc-02") and (args.env != "staging")):
	exit("Error:  -e must be poc-01 or poc-02 only\n")
if args.env == "poc-01":
	sHost="https://ksoc-poc-01.knightscope.internal/ajax/api"
elif args.env == "poc-02":
	sHost="https://ksoc-poc-02.knightscope.internal/ajax/api"
elif args.env == "staging":
	sHost="https://staging.ksoc.co/ajax/api"


################################################
#	Function
############################

# Global Settings
runID = generateTestExecution(sHost, args)

# Data Collection
test_data = extract_dataFiles(component)

# Call runtimeExecutor
execution_results = runTestSuite(sHost, test_data, runID, component)

# Generate results
#generate_results(execution_results)
