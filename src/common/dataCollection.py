import os
import pandas as pd
import json

from src.common.globalSettings import component

# clean up runtime/tmp and retrieve test data for runtime

def extract_dataFiles(component):
    json_files = []
    # Find our json files
    if component is 'all':
        for subdir, dirs, files in os.walk('C:/Users/ask/Documents/QA Automation TestSuite/python3-version/test_data/'):
            for filename in files:
                filepath = subdir + os.sep + filename
                if filepath.endswith('.json'):
                    json_files.append(filepath)
    else:
        path_to_json = 'test_data/' + component + '/'
        json_files = [pos_json for pos_json in os.listdir(path_to_json) if pos_json.endswith('.json')]

    return json_files
