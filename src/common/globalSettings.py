from datetime import datetime
from uuid import uuid4


# set run id or datetime to create uniqueness, read in main.properties, create an extension to allow plugin application properties

sHost = ""
runID = ""
component = "all"   # default to all components

# Default user for poc-02
uname = "ask"
pword = "knight123"


def generateTestExecution(hostUrl, args):
    sHost = hostUrl
    if args.comp:
        runID = datetime.now().strftime('%Y%m%d%H%M%S-') + args.app + '-' + args.comp
        component = args.comp
    else:
        runID = datetime.now().strftime('%Y%m%d%H%M%S-') + args.app + '-all'
    return runID



