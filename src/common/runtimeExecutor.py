import os
import json
import pandas as pd

from src.aut.api.Login import Login
from src.aut.api.User import User
from src.aut.api.Deployment import Deployment
from src.aut.api.Sector import Sector
from src.aut.api.Event import Event
from src.aut.api.ALPR import ALPR
from src.aut.api.FaceRecognition import FaceRecognition
from src.aut.api.Badgerfid import Badgerfid
from src.aut.api.Thermal import Thermal
from src.aut.api.K5 import K5
from src.aut.api.AOD import AOD
from src.aut.api.ASD import ASD
from src.aut.api.Fleet import Fleet
from src.aut.api.Video import Video
from src.aut.api.Audio import Audio
from src.aut.api.KIadmin import KIadmin

# print(pd.show_versions())

def runTestSuite(sHost, test_data, runID, component):

    if sHost == "https://staging.ksoc.co/ajax/api":
        uname = "ask"
        pword = "5dACD4+A9ZxLcpjC"
    else:
        # Default user: ASK
        uname = "ask"
        pword = "knight123"

    print(uname + ":" + pword)

    # Initialize test execution -- LOGIN
    access_token = initializeTest(uname, pword, sHost)
    
    # Create dataframe
    execution_results = pd.DataFrame(columns=['Description','Action','Component','Data','Exp_results','Execution_status', 'Actual_results'])

    # Iterate through json files
    if component == 'all':
        path_to_json = 'test_data/'
    else:
        path_to_json = 'test_data/'+ component + '/'
    for index, js in enumerate(test_data):
        with open(os.path.join(path_to_json, js)) as json_file:
            # print(json_file)
            json_text = json.load(json_file)
            print(json_text['description'])

            # Run test execution
            status, result = executeTest(access_token, sHost, json_text)

            # Document test details
            action = json_text['action']
            component = json_text['component']
            data = json_text['data']
            exp_results = json_text['exp_results']
            description = json_text['description']
            execution_status = 'Pass' if status else 'Fail'
            actual_results = result
            
            # Add test execution details to dataframe
            execution_results.loc[index] = [description, action, component, data, exp_results, execution_status, actual_results]
    
    # Temporarily output results to csv file
    filename = 'output/Test_Execution_'+runID+'.csv'
    with open(filename,'w+') as output:
        output.write(execution_results.to_string(index=False, justify='left'))
    # execution_results.to_csv('results.csv')

    # Close test execution -- LOGOUT
    # closeTest()

    
    return execution_results
            




def initializeTest(uname, pword, sHost):
    login = Login(uname, pword, sHost)
    access_token = login.get_accessToken()
    return access_token


def closeTest():
    # Logout()
    return 0

def executeTest(access_token, sHost, obj):
    components = [
        ('user', User),
        ('deployment', Deployment),
        ('sector', Sector),
        ('event', Event),
        ('faceRec', FaceRecognition),
        ('alpr', ALPR),
        ('badgerfid', Badgerfid),
        ('k5', K5),
        ('aod', AOD),
        ('asd', ASD),
        ('fleet', Fleet),
        ('video', Video),
        ('audio', Audio),
        ('kiadmin', KIadmin)
    ]
    for (comp, name) in components:
        if obj['component'] == comp:
            curr_user = name(access_token, sHost)
            if 'action' in obj and obj['action'] == 'GET':
                (status, results) = curr_user.read(obj['data'], obj['exp_results'])
            elif 'action' in obj and obj['action'] == 'POST':
                (status, results) = curr_user.create(obj['data'], obj['exp_results'])
            elif 'action' in obj and obj['action'] == 'PUT':
                (status, results) = curr_user.update(obj['data'], obj['exp_results'])
            elif 'action' in obj and obj['action'] == 'DELETE':
                (status, results) = curr_user.delete(obj['data'], obj['exp_results'])
    return status, results

    
