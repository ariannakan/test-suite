import pandas as pd
import os
import json
import csv


from src.common.globalSettings import runID


def initialize_dataframe(json_files):
    #Define pandas dataframe with columns
    global jsons_data
    jsons_data = pd.DataFrame(columns=['action','component','description','data','exp_results','results'])


def generate_results(execution_results):
    
    # displaying the DataFrame 
    print(execution_results) 

#def append_to_dataframe(results):




'''
# Find our json files
    path_to_json = 'test_data/'
    # Define my pandas Dataframe with the columns that I want to get from the json
    global jsons_data
    jsons_data = pd.DataFrame(columns=['action','component','description','user','data','exp_results'])

    # we need both the json and an index number so use enumerate()
    for index, js in enumerate(json_files):
        with open(os.path.join(path_to_json, js)) as json_file:
            json_text = json.load(json_file)

            action = json_text['action']
            component = json_text['component']
            data = json_text['data']
            exp_results = json_text['exp_results']
            description = json_text['description']
            if 'user' in json_text:
                user = json_text['user']
            else:
                user = ""

            # push a list of data into a pandas Dataframe at row given by 'index'
            jsons_data.loc[index] = [action, component, description, user, data, exp_results]
'''