import json
import logging

logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')

from .Requests import GETrequest, POSTrequest

class Login():

    def __init__(self, uname, pword, sHost, sClientId=None, sClientSecret=None):
        self.username = uname
        self.password = pword
        self.access_token = ''
        self.sHost = sHost
        self.login(uname, pword)


    def get_accessToken(self):
        return self.access_token


    def login(self, sUserId, sPassword):
        '''
        url('login/', login_view)

        Example URL: https://ksoc-poc-02.knightscope.internal/login/
        2 methods, GET and POST. They use the same URL.
        GET: does not require any parameters or body. Returns HTML for the login page.
        POST: For logging in. Requires a body with key:[value] pairs below:
        'uname': [string with username for login]
        'pword': [string with password for login]
        '''

        print('Attempting to login')
        get_data = {
            '__type__':'GET',
            'url': self.sHost.replace('/ajax/api','/login/'),
            'header': '',
            'cookies':''
        }
        print(get_data['url'])
        get_result = GETrequest(get_data['url'], get_data['header'], get_data['cookies'])
        if get_result.status_code == 200:
            #logging.info('GET Result|'+ str(get_result.status_code))
            csrf_token = get_result.cookies.get('csrftoken')
            #logging.info(csrf_token)
            post_data = {
                '__type__':'POST',
                'url': self.sHost.replace('/ajax/api','/login'),
                'header': '',
                'cookies':'',
                'body':{
                    'csrfmiddlewaretoken':csrf_token,
                    'uname':self.username, 
                    'pword':self.password}
            }
            #print(str(post_data))
            post_result = POSTrequest(post_data['url'], post_data['header'], post_data['cookies'], post_data['body'])
            ## GET ACCESS TOKEN FROM RESPONSE HEADER
            print('Login Result|'+ str(post_result) + '\n')
            self.access_token = post_result.cookies.get_dict()
            return(post_result)
        else:
            return(get_result)
        

    
    def deployment_login(self, ksoc_url):
        '''
        url(r'^(.*?)/login/?', deployment_login_view)

        Example URL: https://ksoc-poc-02.knightscope.internal/QA_SSO/login/
        1 method, GET. This route serves the login HTML for single sign on. For example, a certain deployment, like TESLA, might have a custom sign on page through Microsoft Azure AD.
        GET: Requires 1 parameter, a valid KSOC_url. This KSOC_url must match a deploymentData KSOC_url. Returns HTML for the login page.
        '''
        logging.info('Deployment Login: ' + ksoc_url)
        sURL = 'https://' + sHost + '/' + ksoc_url + '/login/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies':''
        }
        result = testEndpoint(data)
        return(result)



    def admin_site_urls(self):
        '''
        url('admin/', admin.site.urls),
        This url is for accessing the Django Admin Panel.
        '''
        logging.info('Admin_site_urls')
        sURL = 'https://' + sHost + '/admin/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        return(result)



    def logout(self):
        '''
        url(r'logout/', logout_view)

        Example URL: https://ksoc-poc-02.knightscope.internal/logout/
        Method does not matter. No parameters or body required. Deletes tokens from cookies of web browser.
        '''
        logging.info('Logout')
        sURL = 'https://' + sHost + '/logout'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        return(result)


    


#ask = Login('ask', 'knight123')



'''
    def login(self, sUserId, sPassword, sClientId, sClientSecret):
        sLoginURL='https://'+sHost+'/o/token/'
        data={
            '__type__':'POST',
            'url': sLoginURL,
            'header':'',
            'body':{
                'client_id':sClientId, 
                'client_secret':sClientSecret,
                'grant_type':'password',
                'username':sUserId,
                'password':sPassword
            }
        }
        result = testEndpoint(data)
        return result
'''

    