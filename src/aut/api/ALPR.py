from .Requests import GETrequest, POSTrequest, PUTrequest, verifySchema, verifyStatusCode
from ...common.globalSettings import runID

import logging
import json
import pandas as pd


logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')

## This is where the assertions are made

class ALPR():

    def __init__(self, access_token, sHost):
        self.sHost = sHost
        self.access_token = access_token
        self.results = {}


    def read(self, data, exp_results):   #GET
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call GET request and verify that the status code returned is as expected
        response = GETrequest(sURL, data['header'], data['cookies'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        # This try/catch/else block first checks if the response returns a json body. If it does not, then it catches the exception and passes. 
        # If it does, then it calls a try/catch block that validates whether the response json has the correct schema
        try:
            response = response.json()
        except ValueError: #no JSON returned
            print("Executed Successfully! Verified status code only. No response body\n")    # if there is no json body, the following assert should not be called
            return True, self.results
        else:
            if('schema' in exp_results):
                if not verifySchema(response, exp_results):
                    self.results.update({'check_schema':'Fail'})
                    return False, self.results
                else:
                    self.results.update({'check_schema':'Pass'})
        # This try/catch block checks if the response data matches the expected results 
        try:
            response_fields = {
                'deployment',
                'total',
                'count',
                'blacklist',
                'whitelist',
                'exclusion',
                'plate'
            }
            for field in response_fields:
                if(field in exp_results): 
                    assert response[field] == exp_results[field], "Incorrect " + field + ": Expecting " + str(exp_results[field]) + ' but got ' + str(response[field]) + '\n'
                    self.results.update({field:response[field], 'check_'+field:'Pass'})
        except AssertionError as e:
            print(e)
            self.results.update({'assertion error':e})
            return False, self.results
        else:
            self.results.update({'check_assertions':'Pass'})
            print('Successfully Tested\n')
            return True, self.results
        
        
    def create(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call POST request and verify that the status code returned is as expected
        response = POSTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        try:
            response = response.json()
        except ValueError: #no JSON returned
            print("Executed Successfully! Verified status code only. No response body\n")    # if there is no json body, the following assert should not be called
            return True, self.results
        # This try/catch block checks if the response data matches the expected results 
        try:
            response_fields = [
                'success',
                'result',
                'deployment',
                'errorMsg',
                'interval',
                'message',
                'alpr_ID'
            ]
            for field in response_fields:
                if(field in response): 
                    assert response[field] == exp_results[field], "Incorrect " + field + ": Expecting " + str(exp_results[field]) + ' but got ' + str(response[field]) + '\n'
                    self.results.update({field:response[field], 'check_'+field:'Pass'})
        except AssertionError as e:
            self.results.update({'check_creation':'Fail'})
            self.results.update({'assertion error':e})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            if('investigate' in exp_results or 'alpr_ID' in exp_results):
                self.results.update({'check_assertions':'Pass'})
                print('Successfully Tested\n')
                return True, self.results
            else:
                # Ensure that plate creation was successful -- test plate details
                print("GET: New Plate Details")
                plate_number = response['plate_number']
                plate_details = {
                    "url":data['url'] + plate_number,
                    "header":"",
                    "cookies":""
                }
                return self.read(plate_details, exp_results)


    def update(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call PUT request and verify that the status code returned is as expected
        response = PUTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            assert response['success'] == exp_results['success'], "Failed to update plate details"
            self.results.update({'update_response':response['success'], 'check_update':'Pass'})
        except AssertionError as e:
            self.results.update({'check_update':'Fail'})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Ensure that plate update was successful -- test plate details
            print("GET: Updated Plate Details")
            plate_details = {
                "url":data['url'],
                "header":"",
                "cookies":""
            }
            return self.read(plate_details, exp_results)

        


    
        
            