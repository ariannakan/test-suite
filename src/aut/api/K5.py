from .Requests import GETrequest, POSTrequest, PUTrequest, verifySchema, verifyStatusCode
from ...common.globalSettings import runID

import logging
import json
import pandas as pd
from datetime import datetime, timedelta, timezone
import time


logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')

## This is where the assertions are made

class K5():

    def __init__(self, access_token, sHost):
        self.sHost = sHost
        self.access_token = access_token
        self.results = {}


    def read(self, data, exp_results):   #GET
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call GET request and verify that the status code returned is as expected
        response = GETrequest(sURL, data['header'], data['cookies'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        # This try/catch/else block first checks if the response returns a json body. If it does not, then it catches the exception and passes. 
        # If it does, then it calls a try/catch block that validates whether the response json has the correct schema
        try:
            response = response.json()
        except ValueError: #no JSON returned
            print("Executed Successfully! Verified status code only. No response body\n")    # if there is no json body, the following assert should not be called
            return True, self.results
        else:
            if('schema' in exp_results):
                if not verifySchema(response, exp_results):
                    self.results.update({'check_schema':'Fail'})
                    return False, self.results
                else:
                    self.results.update({'check_schema':'Pass'})
        # This try/catch block checks if the response data matches the expected results 
        try:
            response_fields = [
                'deployment', 
                'current_machine',
                'adm_platform',
                'deployment_name',
                'hw_version',
                'active',
                'online',
                'min_name',
                'machines',
                'machineHistory',
                'deployment_ID',
                'ksoc_adm_comms_version',
                'min',
                'adm_type',
                'MIN',
                'on_call',
                'lock',
                'success',
                'passphrase',
            ]
            for field in response_fields:
                if(field in exp_results): 
                    assert response[field] == exp_results[field], "Incorrect " + field + " response: Expecting " + str(exp_results[field]) + ' but got ' + str(response[field]) + '\n'
                    self.results.update({field:response[field], 'check_'+field:'Pass'})
            if('time_stamp' in exp_results):
                date_time_obj = datetime.strptime(response['time_stamp'], '%Y-%m-%d %H:%M:%S.%f%z')
                time_diff = (datetime.now(timezone.utc) - date_time_obj).total_seconds()
                assert  time_diff < 10, "Time diff: " + str(time_diff)
                self.results.update({'time_stamp_diff':time_diff, 'check_time_stamp_diff':'Pass'})
        except AssertionError as e:
            print(e)
            self.results.update({'assertion error':e})
            return False, self.results
        else:
            self.results.update({'check_assertions':'Pass'})
            print('Successfully Tested\n')
            return True, self.results
        
        
    def create(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        if 'audit' in data['body']:
            data['body']['audit'] = json.dumps([data['body']['audit'], str(datetime.now())])
        else:
            data['body']['time_stamp'] = str(datetime.now().timestamp())
            print(data['body']['time_stamp'])
        # Call POST request and verify that the status code returned is as expected
        time.sleep(5)
        response = POSTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            response_fields = [
                'success',
                'MIN'
            ]
            for field in response_fields:
                if(field in response): 
                    assert response[field] == exp_results[field], "Incorrect " + field + ": Expecting " + str(exp_results[field]) + ' but got ' + str(response[field]) + '\n'
                    self.results.update({field:response[field], 'check_'+field:'Pass'})
        except AssertionError as e:
            self.results.update({'check_creation':'Fail'})
            self.results.update({'assertion error':e})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Ensure that machine creation was successful -- test machine details
            if data['url'] == "/k5/12/active/post/":
                time.sleep(5)
                print("GET: New Machine Details")
                min_details = {
                    "url":"/k5/12",
                    "header":"",
                    "cookies":""
                }
                return self.read(min_details, exp_results['response_exp_results'])
            else:
                self.results.update({'check_assertions':'Pass'})
                print('Successfully Tested\n')
                return True, self.results


    def update(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        data['body']['audit'] = json.dumps([data['body']['audit'], str(datetime.now())])
        # print(data['body']['audit'])
        # Call PUT request and verify that the status code returned is as expected
        time.sleep(5)
        response = PUTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            response_fields = [
                ('success','Failed to update plate details'),
                ('MIN','Incorrect MIN'),
            ]
            for field, error_msg in response_fields:
                if(field in response): 
                    assert response[field] == exp_results[field], error_msg
                    self.results.update({field:response[field], 'check_'+field:'Pass'})
        except AssertionError as e:
            self.results.update({'check_update':'Fail'})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Ensure that user update was successful -- test user details
            time.sleep(5)
            print("GET: Updated MIN Details")
            MIN_details = {
                "url":data['url'],
                "header":"",
                "cookies":""
            }
            return self.read(MIN_details, exp_results['response_exp_results'])

        


    
        
            