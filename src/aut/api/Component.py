from .Requests import GETrequest, POSTrequest, PUTrequest, verifySchema, verifyStatusCode


class Component:

    def __init__(self, access_token, sHost):
        self.sHost = sHost
        self.access_token = access_token
        self.results = {}


    def read(self, data, exp_results):   #GET
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call GET request and verify that the status code returned is as expected
        response = GETrequest(sURL, data['header'], data['cookies'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        # This try/catch/else block first checks if the response returns a json body. If it does not, then it catches the exception and passes. 
        # If it does, then it calls a try/catch block that validates whether the response json has the correct schema
        try:
            response = response.json()
        except ValueError: #no JSON returned
            print("Executed Successfully! Verified status code only. No response body\n")    # if there is no json body, the following assert should not be called
            return True, self.results
        else:
            if not verifySchema(response, exp_results):
                self.results.update({'check_schema':'Fail'})
                return False, self.results
            else:
                self.results.update({'check_schema':'Pass'})



    def create(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call POST request and verify that the status code returned is as expected
        response = POSTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})



    def update(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call PUT request and verify that the status code returned is as expected
        response = PUTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})