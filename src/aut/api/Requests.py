import os
import requests
import json
import time
import urllib3
import logging
from jsonschema import validate, Draft7Validator, exceptions

#logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')


################################

def parseResults(self, sType, sResults):
    lResultSplit=sResults.split('|')
    if sType == "code":
        sText=lResultSplit[0]
    elif sType == "time":
        sText = lResultSplit[1]
    elif sType == "response":
        sText = lResultSplit[2]
    elif sType == "header":
        sText = lResultSplit[3]
    
    return sText

def GETrequest(sURL, header, cookies, params=None, sFilter=None):
    startTime=time.time()
    logging.info("url: "+sURL)
    #if header != '': logging.info(header)
    if sFilter:
        response = requests.get(sURL, headers=header, cookies=cookies, params=sFilter, verify=False)
    else:
        response = requests.get(sURL, headers=header, cookies=cookies, verify=False)
    durationTime=time.time() - startTime
    try:
        response.json()
        #return (str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"|"+str(response.headers))
        logging.info(str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"\n")
        return response
    except ValueError: #no JSON returned
        logging.info(str(response.status_code)+"|"+str(durationTime)+"\n")
        return response


def POSTrequest(sURL, header, cookies, body, raw=False):
    # This rquest handles dictionary, bytes, or file-lie objects to send in the body
    startTime=time.time()
    logging.info("sURL: "+sURL)
    #if header != '': logging.info(header)
    logging.info(body)
    if not raw:
        response = requests.post(sURL, data=body, headers=header, cookies=cookies, verify=False, allow_redirects=False)
    else:
        response = requests.post(sURL, json=body, headers=header, cookies=cookies, verify=False)
    logging.info('request completed')
    durationTime=time.time() - startTime
    #return (str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"|"+str(response.headers))
    try:
        response.json()
        if response.encoding is None:
            logging.info(str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"\n")
        else:
            logging.info(str(response.status_code)+"|"+str(durationTime)+"|"+response.text.encode(response.encoding)+"\n")
        return response
    except ValueError:
        logging.info(str(response.status_code)+"|"+str(durationTime)+"\n")
        return response


def PUTrequest(sURL, header, cookies, body, raw=False):
    startTime=time.time()
    logging.info("sURL: "+sURL)
    #if header != '': logging.info(header)
    logging.info(body)
    if not raw:
        response = requests.put(sURL, data=body, headers=header, cookies=cookies, verify=False)
    else:
        response = requests.put(sURL, json=body, headers=header, cookies=cookies, verify=False)
    logging.info('request completed')
    durationTime=time.time() - startTime
    #return (str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"|"+str(response.headers))
    try:
        response.json()
        if response.encoding is None:
            logging.info(str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"\n")
        else:
            logging.info(str(response.status_code)+"|"+str(durationTime)+"|"+response.text.encode(response.encoding)+"\n")
        return response
    except ValueError:
        logging.info(str(response.status_code)+"|"+str(durationTime)+"\n")
        return response


def DELETErequest(sURL, header, cookies, body=None):
    startTime=time.time()
    logging.info("url: "+sURL)
    #if header != '': logging.info(header)
    response = requests.delete(sURL, json=body, headers=header, cookies=cookies, verify=False)
    durationTime=time.time() - startTime
    try:
        response.json()
        #return (str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"|"+str(response.headers))
        logging.info(str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"\n")
        return response
    except ValueError: #no JSON returned
        logging.info(str(response.status_code)+"|"+str(durationTime)+"\n")
        return response


def verifySchema(response, exp_results):
    path_to_schema = 'schemas/'
    schema_test = exp_results['schema']
    schema_file = open(os.path.join(path_to_schema, schema_test), 'r')
    schema_text = json.load(schema_file)
    try:
        Draft7Validator(schema_text).validate(response)
    except exceptions.ValidationError as e:
        print(e)
        return False
    else:
        #print("Successfully passed schema check")
        return True

def verifyStatusCode(response, exp_results):
    # This try/catch block checks that the response status code correlates to the expected results http_code
    try:
        assert response.status_code == int(exp_results['http_code']), "Error: expected response to have status code " + str(exp_results['http_code']) + ' but got ' + str(response.status_code) + '\n'
        return True
    except AssertionError as e:
        print(e)
        return False


'''
def testEndpoint(obj):  #rename to initiateRequest()
    if '__type__' in obj and obj['__type__'] == 'GET':
        logging.info('GET request')
        if 'filter' in obj:
            return GETrequest(obj['url'], obj['header'], obj['cookies'], obj['filter'])
        else:
            return GETrequest(obj['url'], obj['header'], obj['cookies'])
    elif '__type__' in obj and obj['__type__'] == 'POST':
        logging.info('POST request')
        return POSTrequest(obj['url'], obj['header'], obj['cookies'], obj['body'])
    elif '__type__' in obj and obj['__type__'] == 'PUT':
        logging.info('PUT request')
        return PUTrequest(obj['url'], obj['header'], obj['cookies'], obj['body'])
'''
