from .Requests import GETrequest, POSTrequest, PUTrequest, DELETErequest, verifySchema, verifyStatusCode
from ...common.globalSettings import runID

import logging
import json
import pandas as pd


logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')

## This is where the assertions are made

class ASD():

    def __init__(self, access_token, sHost):
        self.sHost = sHost
        self.access_token = access_token
        self.results = {}


    def read(self, data, exp_results):   #GET
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call GET request and verify that the status code returned is as expected
        response = GETrequest(sURL, data['header'], data['cookies'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        # This try/catch/else block first checks if the response returns a json body. If it does not, then it catches the exception and passes. 
        # If it does, then it calls a try/catch block that validates whether the response json has the correct schema
        try:
            response = response.json()
        except ValueError: #no JSON returned
            print("Executed Successfully! Verified status code only. No response body\n")    # if there is no json body, the following assert should not be called
            return True, self.results
        else:
            if not verifySchema(response, exp_results):
                self.results.update({'check_schema':'Fail'})
                return False, self.results
            else:
                self.results.update({'check_schema':'Pass'})
        # This try/catch block checks if the response data matches the expected results
        try:
            if('success' in response): 
                assert response['success'] == exp_results['success'], "Unsuccessful: Expecting " + exp_results['success'] + ' but got ' + response['success'] + '\n'
                self.results.update({'success':response['success'], 'check_success':'Pass'})
        except AssertionError as e:
            print(e)
            self.results.update({'assertion error':e})
            return False, self.results
        else:
            self.results.update({'check_assertions':'Pass'})
            print('Successfully Tested\n')
            return True, self.results
        


    def create(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        request = ""
        # Call POST request and verify that the status code returned is as expected
        if data['url'] == "/deployments/1/asd/blacklist/":
            response = POSTrequest(sURL, data['header'], data['cookies'], data['body'])
        else:
            response = POSTrequest(sURL, data['header'], data['cookies'], data['body'], raw=True)
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            if('success' in response): 
                assert response['success'] == exp_results['success'], "Unsuccessful: Expecting " + exp_results['success'] + ' but got ' + response['success'] + '\n'
                self.results.update({'success':response['success'], 'check_success':'Pass'})
        except AssertionError as e:
            print(e)
            self.results.update({'assertion error':e})
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Ensure that creating new listed mac_address was successful -- test exists
            print("GET: Newly listed mac_address")
            new_blacklist = {
                "url": '/deployments/1/asd/mac/address/?mac_address=' + data['body']['address'],
                "header":"",
                "cookies":""
            }
            return self.read(new_blacklist, exp_results)


    def update(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call PUT request and verify that the status code returned is as expected
        if data['url'] == "/deployments/1/asd/blacklist/770/":
            response = PUTrequest(sURL, data['header'], data['cookies'], data['body'])
        else:
            response = PUTrequest(sURL, data['header'], data['cookies'], data['body'], raw=True)
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            assert response['success'] == exp_results['success'], "Unsuccessful update: Expecting " + exp_results['success'] + ' but got ' + response['success'] + '\n'
            self.results.update({'update_response':response['success'], 'check_update':'Pass'})
        except AssertionError as e:
            self.results.update({'check_update':'Fail'})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Ensure that mac_ID update was successful -- test mac_ID details
            print("GET: Updated Mac_ID Details")
            if data['url'] == "/deployments/1/asd/blacklist/770/":
                new_url = data['url']
            else:
                new_url = '/deployments/1/asd/mac/address/?mac_address=' + data['body']['address']
            mac_ID_details = {
                "url":new_url,
                "header":"",
                "cookies":""
            }
            return self.read(mac_ID_details, exp_results)


    def delete(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call POST request and verify that the status code returned is as expected
        response = DELETErequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            assert response['deleted'] == exp_results['deleted'], "Unsuccessful deletion: Expecting " + exp_results['deleted'] + ' but got ' + response['deleted'] + '\n'
            self.results.update({'delete_response':response['deleted'], 'check_delete':'Pass'})
        except AssertionError as e:
            self.results.update({'check_delete_mac_address':'Fail'})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Ensure that deleting event_generating_schedule was successful -- test if it still exists
            # self.read()
            print('Successfully deleted\n')
            return True, self.results
