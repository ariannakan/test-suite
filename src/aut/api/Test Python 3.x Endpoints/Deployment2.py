import json
import logging

logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')

from Requests import testEndpoint, sHost


################
# Eventually need to return a json of results


class Deployment():

    def __init__(self, deploymentID, access_token):
        self.deploymentID = deploymentID
        self.access_token = access_token


    def get_Chargepad_Loc(self):
        '''
        url(r'^deployments/([^/]*?)/chargepad/?$', chargepad_show)

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/chargepad/
        1 method, GET. Returns JSON related to chargepad for a particular deployment. 
        GET: Requires 1 parameter, a deployment_ID. 
        '''
        logging.info('Chargepad Location for deployment ' + self.deploymentID)
        sURL = 'https://' + sHost + '/deployments/' + self.deploymentID + '/chargepad/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        return(result)


    def get_Deployment_Details(self):
        '''
        url(r'^deployments/([^/]*?)/?$', deployment_show)

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/
        1 method, GET. Returns all the details of a particular deployment. 
        GET: Requires 1 parameter, a deployment_ID. 
        '''
        logging.info('Deployment ' + self.deploymentID + ' Details')
        sURL='https://' + sHost + '/deployments/' + self.deploymentID
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        return(result)

    def get_Machines_Alive(self):
        '''
        url(r'^deployments/([^/]*?)/alive/?$', deployment_machine_alive),

        Example URL: https://ksoc-poc-02.knightscope.internal/1/alive/
        1 method, GET. Returns JSON showing which machines at the deployment_ID are online. 
        GET: Requires 1 parameter, a deployment_ID. 
        '''
        logging.info('Machines Alive for deployment ' + self.deploymentID)
        sURL='https://' + sHost + '/deployments/' + self.deploymentID + '/alive'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        return(result)

    def get_IndoorMap_Boundaries(self):
        '''
        url(r'^deployments/([^/]*?)/indoormap/boundaries/?$', deployment_indoormapbounds),

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/indoormap/boundaries/
        1 method, GET. Returns two coordinates for google maps to ovelay the indoor map. 
        GET: Requires 1 parameter, a deployment_ID. 
        '''
        logging.info('Indoormap boundaries for deployment '+ self.deploymentID)
        sURL='https://' + sHost + '/deployments/' + self.deploymentID + '/indoormap/boundaries/'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        return(result)

    def get_Levels(self):
        '''
        url(r'^deployments/([^/]*?)/levels/?$', deployment_levels)

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/levels/
        1 method, GET. Returns a list of levels in the deployment
        GET: Requires 1 parameter, a deployment_ID
        '''
        logging.info('Levels for deployment '+ self.deploymentID)
        sURL='https://' + sHost + '/deployments/' + self.deploymentID + '/levels/'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        return(result)