import json
from testStructure import testEndpoint, sHost

class Login():

    def __init__(self, uname, pword, sClientId=None, sClientSecret=None):
        self.username = uname
        self.password = pword
        self.access_token = ''
        print('######### Login Controller ##########\n')
        self.login(uname, pword)


    def get_accessToken(self):
        return self.access_token


    def login(self, sUserId, sPassword):
        '''
        url('login/', login_view)

        Example URL: https://ksoc-poc-02.knightscope.internal/login/
        2 methods, GET and POST. They use the same URL.
        GET: does not require any parameters or body. Returns HTML for the login page.
        POST: For logging in. Requires a body with key:[value] pairs below:
        'uname': [string with username for login]
        'pword': [string with password for login]
        '''

        print('Login Test')
        sLoginURL='https://'+sHost+'/login/'
        get_data = {
            '__type__':'GET',
            'url': sLoginURL,
            'header': '',
            'cookies':''
        }
        get_result = testEndpoint(get_data)
        if get_result.status_code == 200:
            print('GET Result|'+ str(get_result.status_code))
            csrf_token = get_result.cookies.get('csrftoken')
            print(csrf_token)
            post_data = {
                '__type__':'POST',
                'url': sLoginURL,
                'header': '',
                'cookies':'',
                'body':{
                    'csrfmiddlewaretoken':csrf_token,
                    'uname':'ask', 
                    'pword':'knight123'}
            }
            post_result = testEndpoint(post_data)
            ## GET ACCESS TOKEN FROM RESPONSE HEADER
            print('POST Result|'+ str(post_result.status_code) + '\n')
            self.access_token = post_result.cookies.get_dict()
            return(post_result)
        else:
            print(get_result)
            return(get_result)
        

    
    def deployment_login(self, ksoc_url):
        '''
        url(r'^(.*?)/login/?', deployment_login_view)

        Example URL: https://ksoc-poc-02.knightscope.internal/QA_SSO/login/
        1 method, GET. This route serves the login HTML for single sign on. For example, a certain deployment, like TESLA, might have a custom sign on page through Microsoft Azure AD.
        GET: Requires 1 parameter, a valid KSOC_url. This KSOC_url must match a deploymentData KSOC_url. Returns HTML for the login page.
        '''
        print('Deployment Login: ' + ksoc_url)
        sURL = 'https://' + sHost + '/' + ksoc_url + '/login'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies':''
        }
        result = testEndpoint(data)
        print(str(result.status_code)+'\n')
        return(result)



    def admin_site_urls(self):
        '''
        url('admin/', admin.site.urls),
        This url is for accessing the Django Admin Panel.
        '''
        print('Admin_site_urls')
        sURL = 'https://' + sHost + '/admin/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)



    def logout(self):
        '''
        url(r'logout/', logout_view)

        Example URL: https://ksoc-poc-02.knightscope.internal/logout/
        Method does not matter. No parameters or body required. Deletes tokens from cookies of web browser.
        '''
        print('######### Logout ##########\n')
        print('Logout')
        sURL = 'https://' + sHost + '/logout'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    


#ask = Login('ask', 'knight123')



'''
    def login(self, sUserId, sPassword, sClientId, sClientSecret):
        sLoginURL='https://'+sHost+'/o/token/'
        data={
            '__type__':'POST',
            'url': sLoginURL,
            'header':'',
            'body':{
                'client_id':sClientId, 
                'client_secret':sClientSecret,
                'grant_type':'password',
                'username':sUserId,
                'password':sPassword
            }
        }
        result = testEndpoint(data)
        return result
'''

    