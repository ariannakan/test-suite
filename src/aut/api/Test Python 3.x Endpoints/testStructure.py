import requests
import json
import time
from datetime import datetime
import os, sys
import argparse
import urllib3

urllib3.disable_warnings()

############################
#Get Start DateTime
#############################
startMainTime=time.time()
startTime=datetime.now().strftime('%Y%m%d_%H%M%S')
print("startTime: "+startTime+"\n")
#############################
parser = argparse.ArgumentParser(description='This is a script by ask.')
parser.add_argument('-e','--env',help='environment to send event', required=True)
args = parser.parse_args()

if ((args.env != "poc-01") and (args.env != "poc-02")):
	exit("Error:  -e must be qa or staging only\n")
if args.env == "poc-01":
	sHost="ksoc-poc-01.knightscope.internal"

elif args.env == "poc-02":
	sHost="ksoc-poc-02.knightscope.internal"

################################


sDeployment="1"
#access_token = {'Cookie': "__token=b'PdufOonALjVDTumqP72VxdcL_TMPoGwRQX0LywSo'; __refresh_token=b'6tpCGobVwIzGOPkPJA9NAfeeNJwyOvaeymt2_reo'; csrftoken=3Cz4t0MDkGqtlexCb33ZhgIVgvQbxbaeZtudKjkSCyPTitcr7gUnceLHtz7ALBzy; sessionid=6i97ibp6shplk4i9t01gctq7uga8lho1"}

def parseResults(self, sType, sResults):
    lResultSplit=sResults.split('|')
    if sType == "code":
        sText=lResultSplit[0]
    elif sType == "time":
        sText = lResultSplit[1]
    elif sType == "response":
        sText = lResultSplit[2]
    elif sType == "header":
        sText = lResultSplit[3]
    
    return sText

def GETrequest(sURL, header, cookies, params=None):
    startTime=time.time()
    print("url: "+sURL)
    #if header != '': print(header)
    if params:
        response = requests.get(sURL, headers=header, cookies=cookies, params=params, verify=False)
    else:
        response = requests.get(sURL, headers=header, cookies=cookies, verify=False)
    response.content
    durationTime=time.time() - startTime
    try:
        response.json()
        #return (str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"|"+str(response.headers))
        return (str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"\n")
    except ValueError: #no JSON returned
        if 'login' in sURL:
            return response
        return (str(response.status_code)+"|"+str(durationTime)+"\n")


def POSTrequest(sURL, header, cookies, body):
    startTime=time.time()
    print("sURL: "+sURL)
    #if header != '': print(header)
    print(body)
    response = requests.post(sURL, data=body, headers=header, cookies=cookies, verify=False)
    print('request completed')
    response.content
    durationTime=time.time() - startTime
    #return (str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"|"+str(response.headers))
    try:
        response.json()
        if response.encoding is None:
            return (str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"\n")
        return (str(response.status_code)+"|"+str(durationTime)+"|"+response.text.encode(response.encoding)+"\n")
    except ValueError:
        if 'login' in sURL:
            return response
        return (str(response.status_code)+"|"+str(durationTime)+"\n")


def PUTrequest(sURL, header, cookies, body):
    startTime=time.time()
    print("sURL: "+sURL)
    #if header != '': print(header)
    print(body)
    response = requests.put(sURL, data=body, headers=header, cookies=cookies, verify=False)
    print('request completed')
    response.content
    durationTime=time.time() - startTime
    #return (str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"|"+str(response.headers))
    try:
        response.json()
        if response.encoding is None:
            return (str(response.status_code)+"|"+str(durationTime)+"|"+response.text+"\n")
        return (str(response.status_code)+"|"+str(durationTime)+"|"+response.text.encode(response.encoding)+"\n")
    except ValueError:
        return (str(response.status_code)+"|"+str(durationTime)+"\n")


def testEndpoint(obj):
    if '__type__' in obj and obj['__type__'] == 'GET':
        print('GET request')
        if 'params' in obj:
            return GETrequest(obj['url'], obj['header'], obj['cookies'], obj['params'])
        else:
            return GETrequest(obj['url'], obj['header'], obj['cookies'])
    elif '__type__' in obj and obj['__type__'] == 'POST':
        print('POST request')
        return POSTrequest(obj['url'], obj['header'], obj['cookies'], obj['body'])
    elif '__type__' in obj and obj['__type__'] == 'PUT':
        print('PUT request')
        return PUTrequest(obj['url'], obj['header'], obj['cookies'], obj['body'])

'''
def generate_accessToken(result):
    cookie_string = "; ".join([str(x)+"="+str(y) for x,y in result.items()])
    #print(cookie_string)
    token = str({'Cookie':cookie_string})
    return token
'''

'''
def convertToJson(result):
    return json.loads(result)
'''

######################################
# Sample 
#####################
sURL = 'https://' + sHost + '/login/'
data = {
    '__type__':'POST',
    'url': sURL,
    'header': '',
    'body':{
        'uname':'ask',
        'pword':'knight123'
    }
}
#print(testEndpoint(data))