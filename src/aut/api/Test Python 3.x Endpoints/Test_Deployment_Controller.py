from Login import *
from Deployment import *

import logging

logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')

##################################################################

logging.info("Testing Deployment Controller\n")

def initialize_deployment_tests(uname, pword, depl_ID):
    # Login and get access token
    login = Login(uname, pword)
    access_token = login.get_accessToken()
    # Create User Instance
    deployment = Deployment(depl_ID, access_token)
    return deployment, login



def test_chargepad(obj):
    resp_cp = obj.get_Chargepad_Loc()
    try:
        assert resp_cp.status_code == 200, "Unable to access deployment information\n"
    except AssertionError as e:
        logging.warning(e)

    resp_cp_body = resp_cp.json()
    try:
        assert resp_cp_body['deployment'] == '1', "Incorrect deployment\n"
        assert resp_cp_body['charge_pad'], "There are no chargepads for this deployment\n"
    except AssertionError as e:
        logging.warning(e)



def test_deployment_details(obj):
    resp_details = obj.get_Deployment_Details()
    try:
        assert resp_details.status_code == 200, "Unable to get deployment information\n"
    except AssertionError as e:
        logging.warning(e)

    resp_details_body = resp_details.json()
    try:
        assert resp_details_body['deployment_ID'] == '1', "Incorrect deployment ID\n"
        assert resp_details_body['deployment_name'] == 'Knightscope HQ', "Incorrect deployment name\n"
        assert resp_details_body['KSOC_url'] == 'KHQ', "Incorrect deployment url\n"
        assert resp_details_body['is_active'] == True, "Deployment is inactive\n"
    except AssertionError as e:
        logging.warning(e)



def test_machines_alive(obj):
    resp_machines = obj.get_Machines_Alive()
    try:
        assert resp_machines.status_code == 200, "Unable to get deployment information\n"
    except AssertionError as e:
        logging.warning(e)
    
    resp_machines_body = resp_machines.json()
    try:
        assert resp_machines_body['deployment_ID'] == '1', "Incorrect deployment\n"
        #assert resp_machines_body['machines']['MIN']
    except AssertionError as e:
        logging.warning(e)


def test_indoormap_boundaries(obj):
    resp_bound = obj.get_IndoorMap_Boundaries()
    try:
        assert resp_bound.status_code == 200, "Unable to get deployment information\n"
    except AssertionError as e:
        logging.warning(e)

    resp_bound_body = resp_bound.json()
    try:
        assert resp_bound_body['coordinates'], "Coordinates do not exist\n"
        assert resp_bound_body['url'], "URL does not exist\n"
    except AssertionError as e:
        logging.warning(e)

    
def test_levels(obj):
    resp_levels = obj.get_Levels()
    try:
        assert resp_levels.status_code == 200, "Unable to get deployment information\n"
    except AssertionError as e:
        logging.warning(e)
    '''
    resp_levels_body = resp_levels.json()
    try:
        assert resp_levels_body['levels'], "Levels do not exist\n"
    except AssertionError as e:
        logging.warning(e)
    '''   

def close(obj):
    resp_logout = obj.logout()
    try:
        assert resp_logout.status_code == 200, "Unable to logout correctly\n"
    except AssertionError as e:
        logging.warning(e)

    

if __name__ == "__main__":
    
    uname = 'ask'
    pword = 'knight123'

    depl_ID = '1'

    #uname = 'akan@wpi.edu'
    #pword = 'knight123'

    deployment, login = initialize_deployment_tests(uname, pword, depl_ID)
    test_chargepad(deployment)
    test_deployment_details(deployment)
    test_machines_alive(deployment)
    test_indoormap_boundaries(deployment)
    test_levels(deployment)
    close(login)