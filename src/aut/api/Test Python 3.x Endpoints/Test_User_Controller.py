from Login import *
from User import *

import logging

logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')

##################################################################
logging.info("Testing User Controller\n")

def initialize_user_tests(uname, pword):
    # Login and get access token
    login = Login(uname, pword)
    access_token = login.get_accessToken()
    # Create User Instance
    user = User(uname, pword, access_token)
    return user, login


def test_user_authentication(obj):
    resp_auth = obj.authenticate_User()
    try:
        assert resp_auth.status_code == 200, "User is not authorized\n"
    except AssertionError as e:
        logging.warning(e)


def test_user_deployments(obj):
    depl_ID = '1'
    resp_depl = obj.getUserDeployments(depl_ID)
    try:
        assert resp_depl.status_code == 200, "Unable to access user information\n"
    except AssertionError as e:
        logging.warning(e)

    resp_depl_body = resp_depl.json()
    try:
        depl_ID = '4'
        assert any(deployment['deployment_ID'] == depl_ID for deployment in resp_depl_body['deployments']), "User does not have access to deployment_ID " + depl_ID + "\n"
    except AssertionError as e:
        logging.warning(e)


def test_user_details(obj):
    resp_details = obj.getUserDetails()
    try:
        assert resp_details.status_code == 200, "Unable to access user information\n"
    except AssertionError as e:
        logging.warning(e)

    resp_details_body = resp_details.json()
    try:
        assert resp_details_body['is_kiadmin'], "User does not have admin status\n"
    except AssertionError as e:
        logging.warning(e)


def test_user_exists(obj):
    username = 'dirk'
    resp_exists = obj.doesUserExist(username)
    try:
        assert resp_exists.status_code == 200, "Unable to access user information\n"
    except AssertionError as e:
        logging.warning(e)

    resp_exists_body = resp_exists.json()
    try:
        assert resp_exists_body['exists'], "User does not exist\n"
    except AssertionError as e:
        logging.warning(e)


def test_machine_auth(obj):
    MIN = '12'
    resp_mach_auth = obj.machine_auth(MIN)
    try:
        assert resp_mach_auth.status_code == 200, "User does not have access to this machine\n"
    except AssertionError as e:
        logging.warning(e)


def test_get_all_users(obj):
    depl_ID = '1'
    resp_all = obj.getAllUsers(depl_ID)
    try:
        resp_all.status_code == 200, "Unable to access user information\n"
    except AssertionError as e:
        logging.warning(e)

    resp_all_body = resp_all.json()
    try:
        uname = 'knightscope'
        assert any(user['username'] == uname for user in resp_all_body['users']), "Username " + uname + " does not exist in deployment " + depl_ID + "\n"
    except AssertionError as e:
        logging.warning(e)


def test_create_user(obj):
    depl_ID = '1'
    email = 'JEF-ree@the-rees.com'
    first_name = 'Jephph'
    last_name = 'Ree'
    depl_list = '1,2'
    resp_create = obj.createUser(depl_ID, email, first_name, last_name, depl_list)
    try:
        resp_create.status_code == 200, "Unable to create new user " + email
    except AssertionError as e:
        logging.warning(e)
    resp_create_body = resp_create.json()
    try:
        resp_create_body['success'] == True, "Failed to create new user"
        resp_create_body['username'] == email, "Incorrect username created"
    except AssertionError as e:
        logging.warning(e)

def test_get_user(obj):
    depl_ID = '1'
    username = 'akan@wpi.edu'
    resp_user = obj.getUser(depl_ID, username)
    try:
        assert resp_user.status_code == 200, "Unable to access user information\n"
    except AssertionError as e:
        logging.warning(e)

    resp_user_body = resp_user.json()
    try:
        assert '1' in resp_user_body['deployment_list']
    except AssertionError as e:
        logging.warning(e)


def test_update_user(obj):
    data={
        'op': 'deployment_list',
        'deployment_list': '1,2,3'
    }
    username = 'akan@wpi.edu'
    depl_ID = '1'
    resp_update = obj.updateUser(depl_ID, username, data)
    try:
        assert resp_update.status_code == 200, "Unable to access user information\n"
    except AssertionError as e:
        logging.warning(e)

    resp_update_body = resp_update.json()
    try:
        assert resp_update_body['success'] == True, "Failure to update user"
        assert resp_update_body['user'] == username, "Incorrect user returned"
    except AssertionError as e:
        logging.warning(e)
    resp_user = obj.getUser(depl_ID, username)
    try:
        assert resp_user.status_code == 200, "Unable to access user information\n"
    except AssertionError as e:
        logging.warning(e)

def close(obj):
    resp_logout = obj.logout()
    try:
        assert resp_logout.status_code == 200, "Unable to logout correctly"
    except AssertionError as e:
        logging.warning(e)




if __name__ == "__main__":
    
    uname = 'ask'
    pword = 'knight123'

    #uname = 'akan@wpi.edu'
    #pword = 'knight123'

    user, login = initialize_user_tests(uname, pword)
    test_user_authentication(user)
    test_user_deployments(user)
    test_user_details(user)
    test_user_exists(user)
    test_machine_auth(user)
    test_get_all_users(user)
    test_create_user(user)
    test_get_user(user)
    test_update_user(user)
    close(login)
'''




ask_login.admin_site_urls()
ask_login.deployment_login('stanford')     #returns 200 but invalid SSO (waiting on SSO deployment to be created)

'''
