import json
from testStructure import testEndpoint, sHost


################
# Eventually need to return a json of results


class Event():

    def __init__(self, deployment_ID, access_token):
        self.deployment_ID = deployment_ID
        self.access_token = access_token
        print('######### Event Controller ##########\n')


    def get_schedule(self, detection_type):
        '''
        url(r'^deployments/([^/]*?)/eventgeneratingschedule/?$', eventController.EventGeneratingSchedule.as_view()),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/eventgeneratingschedule
        2 methods, GET and POST. They use the same URL, with a deployment_id (1 in example)
        GET: Returns list of every location event boundary for that sector as JSON.

        Needs param: detection_type
        '''
        print('EventGeneratingSchedule for deployment ' + self.deployment_ID + '; filtered by ' + detection_type)
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/eventgeneratingschedule/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'params':{'detection_type':detection_type}
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def create_schedule(self, body):
        '''
        url(r'^deployments/([^/]*?)/eventgeneratingschedule/?$', eventController.EventGeneratingSchedule.as_view()),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/eventgeneratingschedule
        2 methods, GET and POST. They use the same URL, with a deployment_id (1 in example)

        POST: Overwrites existing schedule for given day. Requires body below:
        'detection_type': [String from the following choices: 'aod', 'alpr', 'asd', 'thermal']
        'day_of_week': [String from the following choices: 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ]
        'sector_ID': [String of sector_id, example: '1', '2', '3']
        'schedule': [A string representing a list of schedules to over-write for a specific day. Example below]
                    str([
                        {
                            "start_time": 00:00,
                            "end_time": 23:59,
                            "day_of_week": "wednesday",
                            "sector_ID": "1",
                        },
                        {
                            "start_time": "00:00",
                            "end_time": "23:59",
                            "day_of_week": "friday",
                            "sector_ID": "1",
                        },
                    ])
        '''
        print('Create schedule for deployment ' + self.deployment_ID)
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/eventgeneratingschedule/'
        data = {
            '__type__':'POST',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'body':body
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def delete_schedule(self,body):
        '''
        url(r'^deployments/([^/]*?)/eventgeneratingschedule/delete_schedule/post/?$', eventController.event_schedule_delete_post),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/eventgeneratingschedule/delete_schedule/post
        1 method, POST. 1 parameter 1 deployment_ID (1 in example)

        POST: Deletes all eventGenereatingSchedule and LocationEventData documents that match the post body criteria, shown below:
        'detection_type': [String from the following choices: 'aod', 'alpr', 'asd', 'thermal']
        'day_of_week': [String from the following choices: 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ]
        'sector_ID': [String of sector_id, example: '1']
        'deleted_locations_IDs': [A string-list of location_ids to delete in the background], example '[1, 3]'
        '''
        print('Delete schedule for deployment ' + self.deployment_ID)
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/eventgeneratingschedule/delete_schedule/post/'
        data = {
            '__type__':'POST',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'body':body
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def get_events(self,params):
        '''
        url(r'^deployments/([^/]*?)/events/?$', eventController.EventIndexV2.as_view()),

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/events
        3 methods, GET, PUT and POST. They use the same URL, with a deployment_id (1 in example)

        GET: Returns a list of events for a deployment and accepts multiple filters as parameters. All parameters are optional. Possible parameters are shown below:
    
        GET PARAMETERS:
        'page': [integer representing page of results to grab] ex. '1'
        'offset': [integer representing offset from page] ex. '0'
        'detection_types': [array of detection types] ex. ['aod', 'alpr', 'asd', 'thermal']
        'sectors': [String of sector_IDs] ex. '1, 3, 4'
        'machines': [String of machine_IDs] ex. '12, 78, 36'
        'clear': [Boolean] ex. 'true'
        'start_time': [POSIX timestamp] ex. '1600463186'
        'end_time': [POSIX timestamp] ex. '1600463186'
        '''
        print('Get Events for deployment ' + self.deployment_ID + ' with params: ' + str(params))
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/events/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'params':params
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def create_event(self, body):
        '''
        url(r'^deployments/([^/]*?)/events/?$', eventController.EventIndexV2.as_view()),

        POST: Adds a new event to KSOC. Requires body below:
        
        'event_type': [String from the following choices: 'aod', 'alpr', 'asd', 'thermal'], ex. 'alpr'
        'MIN': [String with machine_ID to log event] ex. '12'
        'time_stamp': [String of POSIX Timestamp, number only.] ex. "1600118821"
        '''
        print('Add Event for deployment ' + self.deployment_ID)
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/events/'
        data = {
            '__type__':'POST',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'body':body
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def update_multiple_events(self, body):
        '''
        PUT: Update the events and marks them cleared. Requires body below
        
        'comment': [String with comment], ex. 'false positive, ignore this'
        'clear': [String with True or False] ex. 'true'
        'clear_time_stamp': [String of POSIX Timestamp, number only.] ex. "1600118821"
        'event_IDs': [String of event_ID values separated by commas] ex. "1, 3, 12"
        '''
        print('Update Events for events ' + body['event_IDs'])
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/events/'
        data = {
            '__type__':'PUT',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'body':body
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def get_event_report(self, start_datetime, end_datetime):
        '''
        url(r'^deployments/([^/]*?)/events/([^/]*?)/([^/]*?)/report/?$', eventController.event_report),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/1/events/1600118821/1600119821/report
        1 method, GET. 3 parameters, deployment_ID (1 in example), start_datetime (1600118821 in example) and end_datetime (1600119821 in example)
                    start_datetime and end_datetime are in POSIX timestamp format

        GET: Returns a CSV with a report on all detection events between the timeframe for a specific deployment
        '''
        print('Generate event report between ' + start_datetime + ' and ' + end_datetime)
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/events/' + start_datetime + '/' + end_datetime + '/report/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def get_event_details(self, event_ID):
        '''
        url(r'^deployments/([^/]*?)/events/([0-9]+?)/?$',eventController.EventShowV2.as_view()),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/events/4
        2 methods, GET and PUT. They use the same URL, with a deployment_ID (1 in example) and event_ID (4 in example)

        GET: Returns an event with a particular event ID. No additional body or parameters required
        '''
        print('Event Details for event ' + event_ID)
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/events/' + event_ID + '/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def update_event(self, event_ID, body):
        '''
        url(r'^deployments/([^/]*?)/events/([0-9]+?)/?$',eventController.EventShowV2.as_view()),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/events/4
        2 methods, GET and PUT. They use the same URL, with a deployment_ID (1 in example) and event_ID (4 in example)
        PUT: Updates the event and marks them cleared. Requires body below:
        
        'comment': [String with comment], ex. 'false positive, ignore this'
        'clear': [String with True or False] ex. 'true'
        'clear_time_stamp': [String of POSIX Timestamp, number only.] ex. "1600118821"
        'event_IDs': [String of event_ID values separated by commas] ex. "1, 3, 12"
        'false_positive': [A String with any contents], ex. "true"
        ***NOTE: the 'false_positive' body is optional. The backend expects either 'clear' or 'false_positive'
        '''
        print('Update event ' + event_ID)
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/events/' + event_ID + '/'
        data = {
            '__type__':'PUT',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'body':body
        }
        result = testEndpoint(data)
        print(result)
        return(result)

    def get_events_aggregate(self, params):
        '''
        url(r'^deployments/([^/]*?)/events/aggregate/?$', eventController.events_aggregate),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/events/aggregate
        1 method, GET. They use the same URL, with a deployment_ID (1 in example)

        GET: Returns an event with a particular event ID. start_time and end_time parameters required

        Params: start_time=1427708400&end_time=1599850800&interval=hourly&machines=12&page=1&detection_types=People
        '''
        print('Events aggregate')
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/events/aggregate/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'params':params
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def get_all_eventGeneratingSchedules(self, params):
        '''
        url(r'^deployments/([^/]*?)/events/schedule/?$', eventController.EventScheduleV2.as_view()),

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/events/schedule
        2 methods, GET and POST. They use the same URL, with a deployment_ID (1 in example)

        GET: Returns all eventGeneratingSchedule documents matching criteria in the following params:
        GET PARAMETERS:
        'day_of_week': [String from the following choices: 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ], ex. 'monday'
        'detection_type': [String from the following choices: 'aod', 'alpr', 'asd', 'thermal']
        '''
        print('Get all eventGeneratingSchedules matching params: ' + str(params))
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/events/schedule/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'params':params
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def update_all_eventGeneratingSchedules(self, body):
        '''
        url(r'^deployments/([^/]*?)/events/schedule/?$', eventController.EventScheduleV2.as_view()),

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/events/schedule
        2 methods, GET and POST. They use the same URL, with a deployment_ID (1 in example)

        POST: Saves a schedule for each day in day_of_week for the specified deployment. Requires post body below:
        "detection_type": <string that matches "aod"/"alpr"/"thermal"/"asd">,
        "start_time": <time in format HH:MM:SS>,
        "end_time": <time in format HH:MM:SS>,
        "day_of_week": <array of days to apply the schedule ["monday", "tuesday", ...]>,
        "replace_all": <bool True=replace existing schedule for the given day_of_week, False=do not replace> (optional)
        '''
        print('Update all eventGeneratingSchedules: ' + str(body))
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/events/schedule/'
        data = {
            '__type__':'POST',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'body':body
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def get_location_event_boundaries(self):
        '''
        url(r'^deployments/([^/]*?)/locationeventboundary/?$', eventController.LocationEventBoundaryIndex.as_view()),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/locationeventboundary/
        2 methods, GET and POST. They use the same URL, with a deployment_ID (1 in example)

        GET: Returns list of every location event boundary for that sector
        '''
        print('Get location event boundaries')
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/locationeventboundary/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)

    def create_location_event_boundary(self, body):
        '''
        url(r'^deployments/([^/]*?)/locationeventboundary/?$', eventController.LocationEventBoundaryIndex.as_view()),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/locationeventboundary/
        2 methods, GET and POST. They use the same URL, with a deployment_ID (1 in example)

        POST: Creates a new location event boundary for a given sector. Requires the parameters below:
        "sector_ID": [String with sector_ID], ex. '1'
        "level": [Optional string with level of sector], ex. '2'
        "detection_type": [String from the following choices: 'aod', 'alpr', 'asd', 'thermal'], ex. 'aod'
        "location_name": [String with a location], ex. 'parking lot entrance'
        "metadata": [Optional string with any meta information], ex. 'metadata'
        "bounding_box_json": <-- Elliot is not sure about this one, we'll need to talk to Phil about this when you reach this.
        '''
        print('Create location event boundary')
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/locationeventboundary/'
        data = {
            '__type__':'POST',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'body':body
        }
        result = testEndpoint(data)
        print(result)
        return(result)

    def get_location_event_boundaries_from_locationID(self, location_ID):
        '''
        url(r'^deployments/([^/]*?)/locationeventboundary/([^/]*?)/?$', eventController.LocationEventBoundaryShow.as_view()),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/locationeventboundary/3
        2 methods, GET and POST. They use the same URL, with a deployment_ID (1 in example) and a location_ID (3 in example)

        GET: Returns JSON list of every location event boundary for that sector given a location_ID
        '''
        print('Get location event boundary from location_ID ' + location_ID)
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/locationeventboundary/' + location_ID + '/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)

    def update_location_event_boundary(self, location_ID, body):
        '''
        url(r'^deployments/([^/]*?)/locationeventboundary/([^/]*?)/?$', eventController.LocationEventBoundaryShow.as_view()),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/locationeventboundary/3
        2 methods, GET and PUT. They use the same URL, with a deployment_ID (1 in example) and a location_ID (3 in example)

        PUT: Creates a new location event boundary for a given sector. Requires the parameters below:
        "detection_type": [String from the following choices: 'aod', 'alpr', 'asd', 'thermal'], ex. 'aod'
        "location_name": [String with a location], ex. 'parking lot entrance'
        "metadata": [Optional string with any meta information], ex. 'metadata'
        "bounding_box_json": <-- Elliot is not sure about this one, we'll need to talk to Phil about this when you reach this.
        '''
        print('Update location event boundary from location_ID ' + location_ID)
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/locationeventboundary/' + location_ID + '/'
        data = {
            '__type__':'PUT',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'body':body
        }
        result = testEndpoint(data)
        print(result)
        return(result)

