from Login import Login
from User import User
from Deployment import Deployment



############# Login #####################

uname = 'ask'
pword = 'knight123'

ask_login = Login(uname, pword)
access_token = ask_login.get_accessToken()


# TEST LOGIN
ask_login.admin_site_urls()
ask_login.deployment_login('stanford')  #correct
ask_login.deployment_login('tesla')     #incorrect

# TEST USER
ask = User(uname, pword, access_token)
ask.authenticate_User()
ask.getUserDeployments('1')
ask.getUserDetails()
ask.doesUserExist('ask')
ask.machine_auth('12')
ask.getAllUsers('1')
ask.createUser('1', 'akan@wpi.edu', 'Arianna', 'Kan', '1,2')
data={
    'op':'user_active',
    'is_active':'True'
}
ask.updateUser('1', 'akan@wpi.edu', data)
ask.getUser('1', 'akan@wpi.edu')

# TEST DEPLOYMENT
khq = Deployment('4', access_token)
khq.getChargepadLoc()
khq.getDeploymentDetails()
khq.getIndoorMap_boundaries()
khq.getIndoorMap()              # currently working with deployment 4 only
khq.getLevels()
khq.getMachines_alive()

######## LOGOUT #########

ask_login.logout()