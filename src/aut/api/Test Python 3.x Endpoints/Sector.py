import json
from testStructure import testEndpoint, sHost


################
# Eventually need to return a json of results


class Sector():

    def __init__(self, deployment_ID, sector_ID, access_token):
        self.deployment_ID = deployment_ID
        self.sector_ID = sector_ID
        self.access_token = access_token
        print('######### Sector Controller ##########\n')


    def get_sector_list(self):
        '''
        url(r'^deployments/([^/]*?)/sectors/?$', sector_index),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/1/sectors/
        1 method, GET. Returns the list of sectors in the deployment.
        GET: Requires 1 parameter, a deployment_ID. 
        '''
        print('Sectors for deployment ' + self.deployment_ID)
        sURL = 'https://' + sHost + '/deployments/' + self.deployment_ID + '/sectors/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def get_sector_detail(self):
        '''
        url(r'^deployments/([^/]*?)/sectors/([^/]*?)/?$', sector_show),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/1/sectors/2/
        1 method, GET. Returns information on a single sector_id
        GET: Requires 2 parameters, a deployment_ID (1 in the example) and a sector_ID (2 in the example). 
        '''
        print('Details for sector ' + self.sector_ID + ' in deployment ' + self.deployment_ID)
        sURL = 'https://' + sHost + '/deployments/' + self.deployment_ID + '/sectors/' + self.sector_ID
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)

    def get_sector_boundaries(self):
        '''
        url(r'^deployments/([^/]*?)/sectors/([^/]*?)/boundaries/?$', boundary_index),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/sectors/2/boundaries/
        1 method, GET. Returns the boundaries for a specific sector in a deployment
        GET: Requires 2 parameters, a deployment_ID (1 in the example) and a sector_ID (2 in the example). 
        '''
        print('Boundaries for sector ' + self.sector_ID + ' in deployment ' + self.deployment_ID)
        sURL = 'https://' + sHost + '/deployments/' + self.deployment_ID + '/sectors/' + self.sector_ID + '/boundaries/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)

    def update_sector_boundaries(self, body):
        '''
        url(r'^deployments/([^/]*?)/sectors/([^/]*?)/boundaries/post/?$', update_boundary),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/1/sectors/2/boundaries/post
        1 method, POST. Updates the boundaries for a specific sector in a deployment.
        POST: Requires 2 parameters, a deployment_ID (1 in the example) and a sector_ID (2 in the example).
        Requires a body with key:[value] pairs below:
        'boundary': [An array of 3 coordinates]

        POST: Resets boundary points for a particular sector. A minimum of 3 boundary points must be added. Requires the values below:
        'boundary': [Array of coordinates, need at least 3. See Example below]
        EXAMPLE
        'boundary': [
            {
                latitude: 37.33,
                longitude: 121.88
            },
            {
                latitude: 37.32,
                longitude: 121.84
            },
            {
                latitude: 37.31,
                longitude: 121.80
            },
        ]
        '''
        print('Update boundaries for sector ' + self.sector_ID + ' in deployment ' + self.deployment_ID)
        sURL = 'https://' + sHost + '/deployments/' + self.deployment_ID + '/sectors/' + self.sector_ID + '/boundaries/post'
        data = {
            '__type__':'POST',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'body':body
        }
        result = testEndpoint(data)
        print(result)
        return(result)

    
    def get_boundaries_BoundaryController(self):
        '''
        url(r'^deployments/([^/]*?)/sector/([^/]*?)/boundaries/?$', BoundaryController.as_view()),
    
        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/sector/2/boundaries
        2 methods, GET and POST. They use the same URL and have two parameters, the deployment_ID (1 in example) and sector_ID (2 in example).

        GET: Returns JSON with all boundary points for the given sector_ID
        '''
        print('Get boundaries for sector ' + self.sector_ID + ' in deployment ' + self.deployment_ID)
        sURL = 'https://' + sHost + '/deployments/' + self.deployment_ID + '/sectors/' + self.sector_ID + '/boundaries/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)

    
    def update_boundaries_BoundaryController(self, body):
        '''
         url(r'^deployments/([^/]*?)/sector/([^/]*?)/boundaries/?$', BoundaryController.as_view()),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/sector/2/boundaries
        2 methods, GET and POST. They use the same URL and have two parameters, the deployment_ID (1 in example) and sector_ID (2 in example).

        *The post request is identical to the update_boundary controller above.
        '''
        print('Update boundaries for sector ' + self.sector_ID + ' in deployment ' + self.deployment_ID)
        sURL = 'https://' + sHost + '/deployments/' + self.deployment_ID + '/sectors/' + self.sector_ID + '/boundaries/post'
        data = {
            '__type__':'POST',
            'url': sURL,
            'header': '',
            'cookies': self.access_token,
            'body':body
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def update_sector_volume(self, body):
        '''
        url(r'^deployments/([^/]*?)/sectors/([^/]*?)/volume/?$', volumeControllerV2.as_view()),
        
        Example URL: https://ksoc-poc-02.knightscope.internal/1/sectors/2/volume/
        1 method, PUT. 2 parameters, deployment_ID (1 in example) and sector_ID (2 in example)

        PUT: Sets volume levels for all machines in a particular sector. Requires the values below:
        'patrol_sound': [Integer between 1-100 as string]
        'alerts': [Integer between 1-100 as string]
        'intercom_concierge': [Integer between 1-100 as string]
        EXAMPLE:
        'patrol_sound': '90'
        'alerts': '80'
        'intercom_concierge': '45'
        '''
        print('Update volume for sector ' + self.sector_ID + ' in deployment ' + self.deployment_ID)
        sURL = 'https://' + sHost + '/ajax/deployments/' + self.deployment_ID + '/sectors/' + self.sector_ID + '/volume/'
        data = {
            '__type__':'PUT',
            'url': sURL,
            'header': '', #{'Authorization': 'Bearer gP01lt9pTTqOvTy3txV6N0XEVdvC8V'},
            'cookies': self.access_token,
            'body':body
        }
        result = testEndpoint(data)
        print(result)
        return(result)

