import json
from testStructure import testEndpoint, sHost


################
# Eventually need to return a json of results


class User():

    def __init__(self, uname, pword, access_token):
        self.username = uname
        self.password = pword
        self.access_token = access_token
        print('######### User Controller ##########\n')

    def getAllUsers(self, deploymentID):
        '''
        url(r'^deployments/([^/]*?)/users/?$', userController.UserIndexV2.as_view()),

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/users
        2 methods, GET and POST. They use the same URL and have one parameter, the deployment_id of the current user.
        GET: Returns all users that share deployments with current user, does not list admins.
        '''
        print('Get all users: ' + deploymentID)
        sURL='https://' + sHost + '/ajax/deployments/' + deploymentID + '/users/'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
            }
        result = testEndpoint(data)
        print(result)
        return result

    def createUser(self, deploymentID, email, first_name, last_name, deployment_list):
        '''
        url(r'^deployments/([^/]*?)/users/?$', userController.UserIndexV2.as_view()),

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/users
        2 methods, GET and POST. They use the same URL and have one parameter, the deployment_id of the current user.
        POST: Creates a new user if the current user is an admin. Requires the values below:
            'email': [email for account activation.]
            'username': [enter the same value as email here. No custom usernames allowed when creating users through this route, only emails.]
            'first_name': [string]
            'last_name': [string]
            'deployment_list': [string containing comma separated values of deployments this person can access. A good default is '1' if your current user's default deployment_id is 1]
            EXAMPLE BODY:
            'email': 'elliotsyoung@gmail.com'
            'username': 'elliotsyoung@gmail.com'
            'first_name': 'Elliot'
            'last_name': 'Young'
            'deployment_list': '1,2'
        '''
        print('Create User: ' + first_name + ' ' + last_name)
        sURL='https://' + sHost + '/ajax/deployments/' + deploymentID + '/users/'
        data = {
            '__type__':'POST',
            'url':sURL,
            'header': '',
            'cookies': self.access_token,
            'body': {
                'email': email,
                'username': email,
                'first_name': first_name,
                'last_name':  last_name,
                'deployment_list': deployment_list
            }
        }
        result = testEndpoint(data)
        print(result)
        return result

    def getUser(self, deploymentID, username):
        '''
        url(r'^deployments/([^/]*?)/users/([^/]*?)/?$', userController.UserShowV2.as_view())

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/users/ask
        2 methods, GET and PUT. They use the same URL and have two parameters: The deployment_ID (1 in example) and username (ask in example)
        GET: Returns JSON data describing the username
        '''
        print('User Details for ' + username)
        sURL='https://' + sHost + '/ajax/deployments/' + deploymentID + '/users/' + username + '/'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
            }
        result = testEndpoint(data)
        print(result)
        return result

    def updateUser(self, deploymentID, username, dData):
        '''
        url(r'^deployments/([^/]*?)/users/([^/]*?)/?$', userController.UserShowV2.as_view())

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/users/ask
        PUT: Modifies an existing user with the given request body. There are several "operations" and each operation is distinguished by the 'op' key. All operation types and their data are listed below.
            'op': 'user_active'
            'is_active': [String of true or false] ex. 'True'
            'op': 'accept_terms'
            'terms_and_conditions_accepted': [String of True or False] ex. 'True
            'time_zone': [String of time zone] ex. "PDT"
            'time_stamp': [String of time stamp] ex. "2020-05-06T05:32:06Z"
            'op': 'update_notifications'
            'email': [String of True or False] ex. 'True'
            'voice': [String of True or False] ex. 'True'
            'text': [String of True or False] ex. 'True'
            'phone': [String of True or False] ex. 'True'
            'time_stamp': [String of time stamp] ex. "2020-05-06T05:32:06Z"
            'op': 'change_password'
            'old_pw': [String of old password] ex. 'pokemon'
            'new_pw': [String of new password] ex. 'digimon'
            'new_pw_confirm': [String of new password, must match] ex. 'digimon'
            'op': 'reset_password'
            * No request body is required for reset_password *
            'op': 'depeloyment_list'
            'deployment_list': [String of comma separated values for deployments the user will have access to] ex. '1,2,3'
            'op': 'set_permissions'
            'deployment_list': [String containing JSON object with permissions, see example below for all permission types]
            "{
                "permission" : {
                    "can_view_machine_health" : true,
                    "can_add_license_plate" : true,
                    "can_view_detection_videos" : true,
                    "can_view_device_statistics" : true,
                    "can_make_custom_broadcast" : true,
                    "can_view_list_preferences" : true,
                    "alarm_sound" : true,
                    "can_make_alarm_sound" : true,
                    "can_view_hd_quality_video" : true,
                    "can_view_investigate_page" : true,
                    "can_generate_reports" : true,
                    "can_download_video" : true,
                    "can_edit_patrol_schedule" : true,
                    "can_view_lte_usage" : true,
                    "can_view_about_page" : true,
                    "can_add_events" : true,
                    "can_view_live_video" : true,
                    "can_download_detection_videos" : true,
                    "can_make_video_clip" : true,
                    "can_view_device_top_ten" : true,
                    "can_make_pre_recorded_messages" : true,
                    "can_patrol_stop_pause_machine" : true,
                    "can_view_parking_meter" : true,
                    "can_view_plate_parking_meter" : true,
                    "can_view_recorded_video" : true,
                    "can_initiate_live_audio" : true,
                    "can_view_cleared_events" : true,
                    "can_clear_events" : true,
                    "can_search_detection" : true,
                    "can_view_machine_usage" : true,
                    "can_view_timeline" : true,
                    "can_edit_patrol_sound" : true,
                    "admin" : false,
                    "can_view_number_of_detections" : true,
                    "can_view_multiple_camera_feed" : true,
                    "can_add_mac_addresses" : true,
                    "can_view_video_clip_history" : true,
                    "can_view_control_panel" : true,
                    "can_view_detection_screen" : true,
                    "can_view_patrol_schedule" : true,
                    "can_view_parking_utilization" : true,
                    "can_initiate_intercom_call" : true
                }
            }"
        '''
        print('Update User: ' + username)
        sURL='https://' + sHost + '/ajax/deployments/' + deploymentID + '/users/' + username + '/'
        data = {
            '__type__':'PUT',
            'url':sURL,
            'header': '',
            'cookies': self.access_token,
            'body': dData
        }
        result = testEndpoint(data)
        print(result)
        return result

    def authenticate_User(self):
        '''
        url(r'^user/auth/?$', userController.user_auth)

        Example URL: https://ksoc-poc-02.knightscope.internal/user/auth/
        1 method, GET. Returns an HTTP code of 200 of the user is authenticated. 
        GET: Requires 0 parameters.
        '''
        print('Authenticate User')
        sURL='https://' + sHost + '/ajax/user/auth/'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
            }
        result = testEndpoint(data)
        print(result)
        return result


    def getUserDetails(self):
        '''
        url(r'^user/?$', userController.userRootV2)

        Example URL: https://ksoc-poc-02.knightscope.internal/user/
        1 method, GET. Returns settings and permission set for current user 
        GET: Requires 0 parameters.
        '''
        print('User Settings and Permissions')
        sURL='https://' + sHost + '/ajax/user/'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
            }
        result = testEndpoint(data)
        print(result)
        return result

    def getUserDeployments(self, deploymentID):
        '''
        url(r'^user/deployments/([^/]*?)/user_deployments/?$', userController.user_alldeployments_show)

        Example URL: https://ksoc-poc-02.knightscope.internal/user/deployments/1/user_deployments
        1 method, GET. Returns all deployments that a user has access to. 
        GET: Requires 1 parameter, a deployment_ID (1 in the example) that matches the user's primary deployment_ID. 
        '''
        print('User Deployments')
        sURL='https://' + sHost + '/ajax/user/deployments/' + deploymentID + '/user_deployments/'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
            }
        result = testEndpoint(data)
        print(result)
        return result


    def doesUserExist(self, username):
        '''
        url(r'^user/user_exists/([^/]*?)/?$', userController.user_exists)

        Example URL: https://ksoc-poc-02.knightscope.internal/user/user_exists/elliotyoung
        1 method, GET. Returns JSON of TRUE/FALSE if the username in parameter exists.
        GET: Requires 1 parameter, a username (elliotyoung in the example). 
        '''
        print('User Exists: ' + username)
        sURL='https://' + sHost + '/ajax/user/user_exists/' + username + '/'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
            }
        result = testEndpoint(data)
        print(result)
        return result


    def machine_auth(self, MIN):
        '''
        url(r'^k5/([0-9]+)/auth/?$', userController.machine_auth)

        Example URL: https://ksoc-poc-02.knightscope.internal/k5/12/auth/
        1 method, GET. Returns an HTTP code of success (200) if the logged in user has access to the MIN# specifified as a parameter
        GET: Requires 1 parameter, a MIN# (12 in the example). 
        '''
        print('User Access MIN: ' + MIN)
        sURL='https://' + sHost + '/ajax/k5/' + MIN + '/auth/'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
            }
        result = testEndpoint(data)
        print(result)
        return result



    