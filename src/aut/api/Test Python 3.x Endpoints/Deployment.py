import json
from testStructure import testEndpoint, sHost


################
# Eventually need to return a json of results


class Deployment():

    def __init__(self, deploymentID, access_token):
        self.deploymentID = deploymentID
        self.access_token = access_token
        print('######### Deployment Controller ##########\n')


    def getChargepadLoc(self):
        '''
        url(r'^deployments/([^/]*?)/chargepad/?$', chargepad_show)

        Example URL: https://ksoc-poc-02.knightscope.internal/1/login/
        1 method, GET. Returns JSON related to chargepad for a particular deployment. 
        GET: Requires 1 parameter, a deployment_ID. 
        '''
        print('Chargepad Location for deployment ' + self.deploymentID)
        sURL = 'https://' + sHost + '/deployments/' + self.deploymentID + '/chargepad/'
        data = {
            '__type__':'GET',
            'url': sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def getDeploymentDetails(self):
        '''
        url(r'^deployments/([^/]*?)/?$', deployment_show)

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/
        1 method, GET. Returns all the details of a particular deployment. 
        GET: Requires 1 parameter, a deployment_ID. 
        '''
        print('Deployment ' + self.deploymentID + ' Details')
        sURL='https://' + sHost + '/deployments/' + self.deploymentID + '/'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)

    def getMachines_alive(self):
        '''
        url(r'^deployments/([^/]*?)/alive/?$', deployment_machine_alive),

        Example URL: https://ksoc-poc-02.knightscope.internal/1/alive/
        1 method, GET. Returns JSON showing which machines at the deployment_ID are online. 
        GET: Requires 1 parameter, a deployment_ID. 
        '''
        print('Machines Alive for deployment ' + self.deploymentID)
        sURL='https://' + sHost + '/deployments/' + self.deploymentID + '/alive'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)

    def getIndoorMap_boundaries(self):
        '''
        url(r'^deployments/([^/]*?)/indoormap/boundaries/?$', deployment_indoormapbounds),

        Example URL: https://ksoc-poc-02.knightscope.internal/1/indoormap/boundaries/
        1 method, GET. Returns two coordinates for google maps to ovelay the indoor map. 
        GET: Requires 1 parameter, a deployment_ID. 
        '''
        print('Indoormap boundaries for deployment '+ self.deploymentID)
        sURL='https://' + sHost + '/deployments/' + self.deploymentID + '/indoormap/boundaries/'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)


    def getIndoorMap(self):
        '''  
        url(r'^deployments/([^/]*?)/indoormap/get/?$', deployment_indoormap_get),

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/indoormap/get/
        1 method, GET. Returns an AWS S3 link to a map. See confluence page for more information. https://knightscope.atlassian.net/wiki/spaces/KD/pages/649953310/How+to+add+Indoor+Map+to+KSOC
        GET: Requires 1 parameter, a deployment_ID. 
        '''
        print('Indoormap for deployment '+ self.deploymentID)
        sURL='https://' + sHost + '/deployments/' + self.deploymentID + '/indoormap/get/'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)
        

    def getLevels(self):
        '''
        url(r'^deployments/([^/]*?)/levels/?$', deployment_levels)

        Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/levels/
        1 method, GET. Returns a list of levels in the deployment
        GET: Requires 1 parameter, a deployment_ID
        '''
        print('Levels for deployment '+ self.deploymentID)
        sURL='https://' + sHost + '/deployments/' + self.deploymentID + '/levels'
        data = {
            '__type__':'GET',
            'url':sURL,
            'header': '',
            'cookies': self.access_token
        }
        result = testEndpoint(data)
        print(result)
        return(result)