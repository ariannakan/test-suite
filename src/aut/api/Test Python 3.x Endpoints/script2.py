import json

from Login import Login
from Sector import Sector
from Event import Event

# all requests must end in '/'


###### LOGIN #######
uname = 'ask'
pword = 'knight123'

ask_login = Login(uname, pword)
access_token = ask_login.get_accessToken()


# TEST SECTOR
sector = Sector('1', '1', access_token)
sector.get_sector_list()
sector.get_sector_detail()
sector.get_sector_boundaries()

update_boundaries_body = {
    "boundary": [
    {
        'id':1,
        'latitude': '37.33',
        'longitude': '121.88'
    },
    {
        'id':2,
        'latitude': '37.32',
        'longitude': '121.84'
    },
    {
        'id':3,
        'latitude': '37.31',
        'longitude': '121.80'
    },
]}
sector.update_sector_boundaries(json.dumps(update_boundaries_body))
sector.get_boundaries_BoundaryController()

update_boundaries_body = {
    "boundary": [
    {
        'id':1,
        'latitude': '0',
        'longitude': '0'
    },
    {
        'id':2,
        'latitude': '50',
        'longitude': '50'
    },
    {
        'id':3,
        'latitude': '100',
        'longitude': '100'
    },
]}
sector.update_boundaries_BoundaryController(json.dumps(update_boundaries_body))

update_volume_body={
    'patrol_sound':'36',
    'alerts':'36',
    'intercom_concierge':'36'
}

sector.update_sector_volume(update_volume_body)


# TEST EVENT 
event = Event('1', access_token)

#event.get_schedule('thermal')
event.get_schedule('aod')                 ## Getting 500 error: internal server error
#event.get_schedule('asd')
#event.get_schedule('alpr')

schedule={
    'detection_type':'alpr',
    'day_of_week': 'monday',        # this day of week must be the same as the rest
    'sector_ID': '1',
    'schedule': str([               # if using str(), the inside must use double quotes " "
        {
            "start_time": "00:00:00",
            "end_time": "01:01:01",
            "day_of_week": "monday",
            "sector_ID": "1",
        },
        {
            "start_time": "07:07:07",
            "end_time": "08:08:08",
            "day_of_week": "monday",
            "sector_ID": "1",
        },
    ])
}
event.create_schedule(schedule)

delete_schedule={
    'detection_type':'alpr',
    'day_of_week': 'monday',
    'sector_ID': '1',
    'deleted_locations_IDs': '[1,3]'
}
event.delete_schedule(delete_schedule)



add_event_body={
    'event_type':'thermal',
    'MIN':'12',
    'time_stamp':'1600341300'
}
event.create_event(add_event_body)

params={'machines':'12'}
params={'clear':True}
params={'start_time':'1600463186'}
#params={'sectors':'0'}
params={'detection_type':'thermal','machines':'12'}
event.get_events(params)


update_events={
    'comment':'testing update 2',
    'clear':'false',
    'clear_time_stamp':'1600341300',
    'event_IDs':'22040'
}
event.update_multiple_events(update_events)

event.get_event_report('1579759917000','1599779917000')

event.get_event_details('22040')
#event.get_event_details('1')

update_event={
    'comment':'false positive, ignore this',
    'clear':'true',
    'clear_time_stamp':'1600341300',
    'event_IDs':'22040',
    'false_positive':'true'
}
event.update_event('22040', update_event)

event.get_event_details('22040')

params={
    'start_time':'1427708400',
    'end_time':'1599850800',
    'interval':'hourly',
    'machines':'12',
    'detection_types':'People'
}
event.get_events_aggregate(params)

event.get_location_event_boundaries()

location_event_boundary_body={
    'sector_ID':'1',
    'level':'1',
    'detection_type':'alpr',
    'location_name':'knightscope parking lot',
    'bounding_box_json':''
}
event.create_location_event_boundary(location_event_boundary_body)

event.get_location_event_boundaries_from_locationID('5f63ce58811f4c4c69e14937')

body={
    "detection_type": 'alpr',
    "location_name": 'knightscope entrance',
    "bounding_box_json": ''
}
event.update_location_event_boundary('5f63ce58811f4c4c69e14937', body)

params={
    'day_of_week':'tuesday',
    'detection_type':'alpr'
}
event.get_all_eventGeneratingSchedules(params)


body={
    'detection_type':'asd',
    'start_time':'00:00:00',
    'end_time':'01:01:01',
    'day_of_week':'["tuesday"]',
    'replace_all':True
}
event.update_all_eventGeneratingSchedules(body)


ask_login.logout()