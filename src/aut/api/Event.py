from .Requests import GETrequest, POSTrequest, PUTrequest, verifySchema, verifyStatusCode
from ...common.globalSettings import runID

import logging
import json
import pandas as pd


logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')

## This is where the assertions are made

class Event():

    def __init__(self, access_token, sHost):
        self.sHost = sHost
        self.access_token = access_token
        self.results = {}


    def read(self, data, exp_results):   #GETc
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call GET request and verify that the status code returned is as expected
        response = GETrequest(sURL, data['header'], data['cookies'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        # This try/catch/else block first checks if the response returns a json body. If it does not, then it catches the exception and passes. 
        # If it does, then it calls a try/catch block that validates whether the response json has the correct schema
        try:
            response = response.json()
        except ValueError: #no JSON returned
            print("Executed Successfully! Verified status code only. No response body\n")    # if there is no json body, the following assert should not be called
            return True, self.results
        else:
            if not verifySchema(response, exp_results):
                self.results.update({'check_schema':'Fail'})
                return False, self.results
            else:
                self.results.update({'check_schema':'Pass'})
        # This try/catch block checks if the response data matches the expected results
        try:
            if('success' in response): 
                assert response['success'] == exp_results['success'], "Unsuccessful: Expecting " + exp_results['success'] + ' but got ' + response['success'] + '\n'
                self.results.update({'success':response['success'], 'check_success':'Pass'})
            if('event_generating_schedule' in response): 
                for schedule in response['event_generating_schedule']:
                    assert schedule['detection_type'] == exp_results['detection_type'], "Incorrect detection_type: Expecting " + exp_results['detection_type'] + ' but got ' + schedule['detection_type'] + '\n'
                    self.results.update({'detection_type':schedule['detection_type'], 'check_detection_type':'Pass'})
            if('event' in response): #singuler event
                response = response['event']
                assert response['event_type'] == exp_results['event_type'], "Incorrect event_type: Expecting " + exp_results['event_type'] + ' but got ' + response['event_type'] + '\n'
                self.results.update({'event_type':response['event_type'], 'check_event_type':'Pass'})
                assert response['MIN'] == exp_results['MIN'], "Incorrect MIN: Expecting " + str(exp_results['MIN']) + ' but got ' + str(response['MIN']) + '\n'
                self.results.update({'MIN':response['MIN'], 'check_MIN':'Pass'})
                assert response['detection_type'] == exp_results['detection_type'], "Incorrect detection_type: Expecting " + exp_results['detection_type'] + ' but got ' + response['detection_type'] + '\n'
                self.results.update({'detection_type':response['detection_type'], 'check_detection_type':'Pass'})
                assert response['created_by'] == exp_results['created_by'], "Incorrect created_by: Expecting " + exp_results['created_by'] + ' but got ' + response['created_by'] + '\n'
                self.results.update({'created_by':response['created_by'], 'check_created_by':'Pass'})
                assert response['comment'] == exp_results['comment'], "Incorrect comment: Expecting " + exp_results['comment'] + ' but got ' + response['comment'] + '\n'
                self.results.update({'comment':response['comment'], 'check_comment':'Pass'})
                assert response['false_positive'] == exp_results['false_positive'], "Incorrect false_positive: Expecting " + str(exp_results['false_positive']) + ' but got ' + str(response['false_positive']) + '\n'
                self.results.update({'false_positive':response['false_positive'], 'check_false_positive':'Pass'})
            if('result' in response): # events aggregate
                for aggregate in response['result']:
                    assert aggregate['type'] == exp_results['type'], "Incorrect type: Expecting " + exp_results['type'] + ' but got ' + aggregate['type'] + '\n'
                    self.results.update({'aggregate_type':aggregate['type'], 'check_type':'Pass'})
        except AssertionError as e:
            print(e)
            self.results.update({'assertion error':e})
            return False, self.results
        else:
            self.results.update({'check_assertions':'Pass'})
            print('Successfully Tested\n')
            return True, self.results
        
    


    def create(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        curr = ""
        # Call POST request and verify that the status code returned is as expected
        response = POSTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            if(exp_results['type'] == "event"): 
                assert response['success'] == exp_results['success'], "Failed to create event"
                self.results.update({'create_event_response':response['success'], 'check_create_event':'Pass'})
                curr = 'event'
                event_id = response['event_ID']
            if(exp_results['type'] == "schedule"): 
                assert response['success'] == exp_results['success'], "Failed to create event_generating_schedule"
                self.results.update({'create_event_generating_schedule_response':response['success'], 'check_create_event_generating_schedule':'Pass'})
                curr = 'schedule'
            if(exp_results['type'] == "LocationEventBoundary"): 
                assert response['success'] == exp_results['success'], "Failed to create LocationEventBoundary"
                self.results.update({'create_location_event_boundary_response':response['success'], 'check_create_location_event_boundary':'Pass'})
                curr = 'boundary'
                boundary_id = response['event_location']
        except AssertionError as e:
            print(e)
            self.results.update({'assertion error':e})
            return False, self.results
        else:
            # print('Successfully passed assertions')
            if curr == 'event':
                # Ensure that creating new event was successful -- test event exists
                print("GET: New Event")
                new_event = {
                    "url":"/deployments/1/events/"+str(event_id),
                    "header":"",
                    "cookies":""
                }
                return self.read(new_event, exp_results['response_exp_results'])
            elif curr == 'schedule':
                # Ensure that creating new event schedule was successful -- test schedule exists
                print("GET: New Event Schedule")
                new_schedule = {
                    "url":data['url'] + '?detection_type=aod',
                    "header":"",
                    "cookies":""
                }
                return self.read(new_schedule, exp_results)
            elif curr == 'boundary':
                # Ensure that creating new locationeventboundary was successful -- test exists
                print("GET: New Location Event Boundary")
                new_boundary = {
                    "url":data['url'] + boundary_id,
                    "header":"",
                    "cookies":""
                }
                return self.read(new_boundary, exp_results)


    def delete(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call POST request and verify that the status code returned is as expected
        response = POSTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            assert response['success'] == exp_results['success'], "Failed to delete event_generating_schedule"
            self.results.update({'delete_event_generating_schedule_response':response['success'], 'check_delete_event_generating_schedule':'Pass'})
        except AssertionError as e:
            self.results.update({'check_delete_event_generating_schedule':'Fail'})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Ensure that deleting event_generating_schedule was successful -- test if it still exists
            # self.read()
            print('Successfully deleted\n')
            return True, self.results


    def update(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call PUT request and verify that the status code returned is as expected
        response = PUTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            assert response['success'] == exp_results['success'], "Failed to update event"
            self.results.update({'update_event_response':response['success'], 'check_update_event':'Pass'})
        except AssertionError as e:
            self.results.update({'check_update_event':'Fail'})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Ensure that updating new event was successful -- test event exists
            print("GET: Updated Event")
            if data['url'] == "/deployments/1/events/":
                event_ID = str(exp_results['response_exp_results']['event_ID'])
                new_event = {
                    "url":"/deployments/1/events/"+event_ID,
                    "header":"",
                    "cookies":""
                }
            else:
                new_event = {
                    "url":data['url'],
                    "header":"",
                    "cookies":""
                }
            return self.read(new_event, exp_results['response_exp_results'])