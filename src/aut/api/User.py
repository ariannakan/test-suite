from .Requests import GETrequest, POSTrequest, PUTrequest, verifySchema, verifyStatusCode
from ...common.globalSettings import runID

import logging
import json
import pandas as pd
from datetime import datetime


logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')

## This is where the assertions are made

class User():

    def __init__(self, access_token, sHost):
        self.sHost = sHost
        self.access_token = access_token
        self.results = {}


    def read(self, data, exp_results):   #GET
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call GET request and verify that the status code returned is as expected
        response = GETrequest(sURL, data['header'], data['cookies'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        # This try/catch/else block first checks if the response returns a json body. If it does not, then it catches the exception and passes. 
        # If it does, then it calls a try/catch block that validates whether the response json has the correct schema
        try:
            response = response.json()
        except ValueError: #no JSON returned
            print("Executed Successfully! Verified status code only. No response body\n")    # if there is no json body, the following assert should not be called
            return True, self.results
        else:
            if not verifySchema(response, exp_results):
                self.results.update({'check_schema':'Fail'})
                return False, self.results
            else:
                self.results.update({'check_schema':'Pass'})
        # This try/catch block checks if the response data matches the expected results 
        try:
            response_fields = {
                'username',
                'email',
                'deployment_ID',
                'is_kiadmin',
                'deployment_list',
                'exists',
                'deployments',
                'email_notifications',
                'text_notifications',
                'voice_notifications',
                'ksoc_version'
            }
            for field in response_fields:
                if(field in response): 
                    assert response[field] == exp_results[field], "Incorrect " + field + ": Expecting " + str(exp_results[field]) + ' but got ' + str(response[field]) + '\n'
                    self.results.update({field:response[field], 'check_'+field:'Pass'})
            if('permissions' in response):
                assert response['permissions']['admin'] == exp_results['permissions']['admin'], "Incorrect admin permissions: Expecting " + str(exp_results['permissions']['admin']) + ' but got ' + str(response['permissions']['admin'])
                self.results.update({'admin_permissions':response['permissions']['admin'], 'check_admin_permission':'Pass'})
            if 'users' in response:
                count = 0
                for user in exp_results['users']:
                    assert user in response['users'], "Incorrect user: " + str(user) + "\n"
                    self.results.update({'user'+str(count):str(user), 'check_user_'+str(count):'Pass'})
                    count += 1
        except AssertionError as e:
            print(e)
            self.results.update({'assertion error':e})
            return False, self.results
        else:
            self.results.update({'check_assertions':'Pass'})
            print('Successfully Tested\n')
            return True, self.results
        
        
    def create(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call POST request and verify that the status code returned is as expected
        response = POSTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            if(response.status_code == 500):
                self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
                print('Successfully tested -- user exists:500\n')
                return True, self.results
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            if('success' in response):
                assert response['success'] == exp_results['success'], "Failed to create user"
                self.results.update({'create_response':response['success'], 'check_creation':'Pass'})
        except AssertionError as e:
            self.results.update({'check_creation':'Fail'})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Ensure that user creation was successful -- test user details
            print("GET: New User Details")
            new_user = data['body']['username'].lower()
            user_details = {
                "url":data['url'] + new_user,
                "header":"",
                "cookies":""
            }
            return self.read(user_details, exp_results)


    def update(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        data['time_stamp'] = datetime.now()
        # Call PUT request and verify that the status code returned is as expected
        response = PUTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            assert response['success'] == exp_results['success'], "Failed to update user"
            self.results.update({'update_response':response['success'], 'check_update':'Pass'})
        except AssertionError as e:
            self.results.update({'check_update':'Fail'})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Ensure that user update was successful -- test user details
            print("GET: Updated User Details")
            new_user = exp_results['user']
            user_details = {
                "url":data['url'],
                "header":"",
                "cookies":""
            }
            return self.read(user_details, exp_results)

        


    
        
            