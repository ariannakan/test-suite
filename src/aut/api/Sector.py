from .Requests import GETrequest, POSTrequest, PUTrequest, verifySchema, verifyStatusCode
from ...common.globalSettings import runID

import logging
import json
import pandas as pd


logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')

## This is where the assertions are made

class Sector():

    def __init__(self, access_token, sHost):
        self.sHost = sHost
        self.access_token = access_token
        self.results = {}


    def read(self, data, exp_results):   #GETc
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call GET request and verify that the status code returned is as expected
        response = GETrequest(sURL, data['header'], data['cookies'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        # This try/catch/else block first checks if the response returns a json body. If it does not, then it catches the exception and passes. 
        # If it does, then it calls a try/catch block that validates whether the response json has the correct schema
        try:
            response = response.json()
        except ValueError: #no JSON returned
            print("Executed Successfully! Verified status code only. No response body\n")    # if there is no json body, the following assert should not be called
            return True, self.results
        else:
            if not verifySchema(response, exp_results):
                self.results.update({'check_schema':'Fail'})
                return False, self.results
            else:
                self.results.update({'check_schema':'Pass'})
        # This try/catch block checks if the response data matches the expected results
        try:
            if type(response) is list:
                if exp_results['schema'] == 'sector_boundaries.json':
                    count = 0
                    for boundary in response:
                        assert boundary in exp_results['boundaries'], "Incorrect boundary: " + str(boundary) + "\n"
                        self.results.update({'boundary_'+str(count):str(boundary), 'check_boundary_'+str(count):'Pass'})
                        count += 1
                if exp_results['schema'] == 'sector_list.json':
                    count = 0
                    for sector in response:
                        assert sector in exp_results['sectors'], "Incorrect sector list: " + str(sector) + "\n"
                        self.results.update({'sector_'+str(count):str(sector), 'check_sector_'+str(count):'Pass'})
                        count += 1
            response_fields = {
                'deployment',
                'sector_ID',
                'deployment_zone_ID',
                'response_name',
                'information',
                'levels',
                'machine_data'
            }
            for field in response_fields:
                if(field in response): 
                    assert response[field] == exp_results[field], "Incorrect " + field + ": Expecting " + str(exp_results[field]) + ' but got ' + str(response[field]) + '\n'
                    self.results.update({field:response[field], 'check_'+field:'Pass'})
        except AssertionError as e:
            print(e)
            self.results.update({'assertion error':e})
            return False, self.results
        else:
            self.results.update({'check_assertions':'Pass'})
            print('Successfully Tested\n')
            return True, self.results



    def create(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call POST request and verify that the status code returned is as expected
        response = POSTrequest(sURL, data['header'], data['cookies'], data['body'], raw=True)
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            assert response['success'] == exp_results['success'], "Failed to create sector boundary\n"
            self.results.update({'create_boundaries_response':response['success'], 'check_create_boundaries':'Pass'})
        except AssertionError as e:
            self.results.update({'assertion error':e})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Ensure that updating boundary points was successful -- test sector boundary points
            print("GET: New Boundary Points")
            new_boundaries = {
                "url":data['url'].replace("/post/","/"),
                "header":"",
                "cookies":""
            }
            return self.read(new_boundaries, exp_results)


    def update(self, data, exp_results):
        # This update method for Sector is only for updating the volume controller
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call PUT request and verify that the status code returned is as expected
        response = PUTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            response_fields = {
                'success',
                'sector_ID',
                'successful_machines',
                'failed_machines'
            }
            for field in response_fields:
                if(field in response): 
                    assert response[field] == exp_results[field], "Incorrect " + field + ": Expecting " + str(exp_results[field]) + ' but got ' + str(response[field]) + '\n'
                    self.results.update({'update_volume_'+field:response[field], 'check_'+field:'Pass'})
        except AssertionError as e:
            self.results.update({'assertion error':e})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Verify that volumes have been updated correctly
            print("GET: Updated Volume")
            updated_volume = {
                "url":"https://ksoc-poc-02.knightscope.internal/ajax/api/adm/12/",
                "header":"",
                "cookies":""
            }
            vol_response = GETrequest(updated_volume['url'], updated_volume['header'], self.access_token)
            print(updated_volume['url'])
            if not verifyStatusCode(vol_response, exp_results):
                self.results.update({'status_code':vol_response.status_code,'check_status_code':'Fail'})
                return False, self.results
            else:
                self.results.update({'status_code':vol_response.status_code,'check_status_code':'Pass'})
            vol_response = vol_response.json()
            response_fields = {
                'patrol_sound_volume',
                'intercom_concierge_volume',
                'alerts_volume'
            }
            for field in response_fields:
                if(field in vol_response): 
                    assert vol_response[field] == exp_results[field], "Incorrect " + field + ": Expecting " + str(exp_results[field]) + ' but got ' + str(vol_response[field]) + '\n'
                    self.results.update({'update_volume_'+field:vol_response[field], 'check_'+field:'Pass'})
            self.results.update({'check_assertions':'Pass'})
            print('Successfully Tested\n')
            return True, self.results