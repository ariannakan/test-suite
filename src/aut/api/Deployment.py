from .Requests import GETrequest, POSTrequest, PUTrequest, verifyStatusCode, verifySchema
from ...common.globalSettings import runID

import logging
import json
import pandas as pd


logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')

## This is where the assertions are made

class Deployment():

    def __init__(self, access_token, sHost):
        self.sHost = sHost
        self.access_token = access_token
        self.results = {}


    def read(self, data, exp_results):   #GETc
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call GET request and verify that the status code returned is as expected
        response = GETrequest(sURL, data['header'], data['cookies'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        # This try/catch/else block first checks if the response returns a json body. If it does not, then it catches the exception and passes. 
        # If it does, then it calls a try/catch block that validates whether the response json has the correct schema
        try:
            response = response.json()
        except ValueError: #no JSON returned
            print("Executed Successfully! Verified status code only. No response body\n")    # if there is no json body, the following assert should not be called
            return True, self.results
        else:
            if not verifySchema(response, exp_results):
                self.results.update({'check_schema':'Fail'})
                return False, self.results
            else:
                self.results.update({'check_schema':'Pass'})
        # This try/catch block checks if the response data matches the expected results 
        try:
            response_fields = {
                'charge_pad',
                'deployment_ID',
                'deployment_name',
                'deployment_address',
                'is_active',
                'KSOC_url',
                'machines',
                'coordinates',
                'url'
            }
            for field in response_fields:
                if(field in response): 
                    assert response[field] == exp_results[field], "Incorrect " + field + ": Expecting " + str(exp_results[field]) + ' but got ' + str(response[field]) + '\n'
                    self.results.update({field:response[field], 'check_'+field:'Pass'})
            if('1' in response): 
                assert response == exp_results['levels'], "Incorrect levels: Expecting " + exp_results['levels'] + ' but got ' + response + '\n'
                self.results.update({'levels':response, 'check_deployment_levels':'Pass'})
        except AssertionError as e:
            print(e)
            self.results.update({'assertion error':e})
            return False, self.results
        else:
            self.results.update({'check_assertions':'Pass'})
            print('Successfully Tested\n')
            return True, self.results