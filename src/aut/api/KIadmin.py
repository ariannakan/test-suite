from .Requests import GETrequest, POSTrequest, PUTrequest, verifySchema, verifyStatusCode
from ...common.globalSettings import runID

import logging
import json
import pandas as pd


logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')

## This is where the assertions are made

class KIadmin():

    def __init__(self, access_token, sHost):
        self.sHost = sHost
        self.access_token = access_token
        self.results = {}


    def read(self, data, exp_results):   #GET
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call GET request and verify that the status code returned is as expected
        response = GETrequest(sURL, data['header'], data['cookies'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        # This try/catch/else block first checks if the response returns a json body. If it does not, then it catches the exception and passes. 
        # If it does, then it calls a try/catch block that validates whether the response json has the correct schema
        try:
            response = response.json()
        except ValueError: #no JSON returned
            print("Executed Successfully! Verified status code only. No response body\n")    # if there is no json body, the following assert should not be called
            return True, self.results
        else:
            if not verifySchema(response, exp_results):
                self.results.update({'check_schema':'Fail'})
                return False, self.results
            else:
                self.results.update({'check_schema':'Pass'})
                print('Successfully Tested\n')
                return True, self.results
        
    


    def create(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call POST request and verify that the status code returned is as expected
        response = POSTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
        except ValueError: #no JSON returned
            print("Executed Successfully! Verified status code only. No response body\n")    # if there is no json body, the following assert should not be called
            return True, self.results
        
        # This try/catch block checks if the response data matches the expected results
        try:
            if('success' in response): 
                assert response['success'] == exp_results['success'], "Unsuccessful: Expecting " + exp_results['success'] + ' but got ' + response['success'] + '\n'
                self.results.update({'success':response['success'], 'check_success':'Pass'})
            if('message' in response): 
                assert response['message'] == exp_results['message'], "Unsuccessful: Expecting " + exp_results['message'] + ' but got ' + response['message'] + '\n'
                self.results.update({'message':response['message'], 'check_message':'Pass'})
        except AssertionError as e:
            print(e)
            self.results.update({'assertion error':e})
            return False, self.results
        else:
            self.results.update({'check_assertions':'Pass'})
            print('Successfully Tested\n')
            return True, self.results
            