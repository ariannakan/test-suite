from .Requests import GETrequest, POSTrequest, PUTrequest, DELETErequest, verifySchema, verifyStatusCode
from ...common.globalSettings import runID

import logging
import json
import pandas as pd
from datetime import datetime

logging.basicConfig(format='%(message)s', filename='logFile.log', filemode='w', level='INFO')

## This is where the assertions are made

class FaceRecognition():

    def __init__(self, access_token, sHost):
        self.sHost = sHost
        self.access_token = access_token
        self.results = {}


    def read(self, data, exp_results):   #GETc
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call GET request and verify that the status code returned is as expected
        response = GETrequest(sURL, data['header'], data['cookies'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
            if response.status_code == 404:
                print("Successfully tested -- image deleted, does not exist anymore\n")
                return True, self.results
        # This try/catch/else block first checks if the response returns a json body. If it does not, then it catches the exception and passes. 
        # If it does, then it calls a try/catch block that validates whether the response json has the correct schema
        try:
            response = response.json()
        except ValueError: #no JSON returned
            print("Executed Successfully! Verified status code only. No response body\n")    # if there is no json body, the following assert should not be called
            return True, self.results
        else:
            if 'schema' in exp_results:
                if not verifySchema(response, exp_results):
                    self.results.update({'check_schema':'Fail'})
                    return False, self.results
                else:
                    self.results.update({'check_schema':'Pass'})
        # This try/catch block checks if the response data matches the expected results 
        try:
            response_fields = {
                'deployment',
                'face_ID',
                'error',
                'images',
                'image',
            }
            for field in response_fields:
                if(field in response): 
                    assert response[field] == exp_results[field], "Incorrect " + field + ": Expecting " + str(exp_results[field]) + ' but got ' + str(response[field]) + '\n'
                    self.results.update({field:response[field], 'check_'+field:'Pass'})
            if('face' in response):
                response = response['face']
                response_fields = [
                    'images',
                    'face_ID',
                    'listed_by',
                    'listing',
                    'name',
                    'listed_reason'
                ]
                for field in response_fields:
                    if(field in response): 
                        assert response[field] == exp_results[field], "Incorrect " + field + ": Expecting " + str(exp_results[field]) + ' but got ' + str(response[field]) + '\n'
                        self.results.update({field:response[field], 'check_'+field:'Pass'})
            if('faces' in response):
                exp_faces = exp_results['faces']
                count = 0
                for face in exp_faces:
                    assert face in response['faces'], "Incorrect face: " + str(face) + "\n"
                    self.results.update({'face_'+str(count):face, 'check_face_'+str(count):'Pass'})
                    count += 1   
        except AssertionError as e:
            print(e)
            self.results.update({'assertion error':e})
            return False, self.results
        else:
            self.results.update({'check_assertions':'Pass'})
            print('Successfully Tested\n')
            return True, self.results
        
        
    def create(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        request = ""
        print('url: ' + sURL)
        # Call POST request and verify that the status code returned is as expected
        response = POSTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            if('success' in response):
                assert response['success'] == exp_results['success'], "POST request failed\n"
                self.results.update({'create_response':response['success'], 'check_creation_success':'Pass'})
                if response['success'] == False:
                    assert response['reason'] == exp_results['reason'], "Incorrect failure reason: Expecting " + exp_results['reason'] + ' but got ' + response['reason'] + '\n'
                    self.results.update({'create_response_failure_reason':response['reason'], 'check_creation_failure_reason':'Pass'})
                    print("Successfully tested: Cannot upload image without a face\n")
                    return True, self.results

                if 'image' in data['body']:
                    request = "add_image"
                else:
                    request = "new_face"
        except AssertionError as e:
            self.results.update({'check_creation':'Fail'})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            print(request)
            if request == "new_face":
                # Ensure that face creation was successful -- test face details
                print("GET: New FaceRec Details")
                face_ID = response['face_ID']
                print(face_ID)
                exp_results['face_ID'] = face_ID
                face_details = {
                    "url":data['url'] + face_ID,
                    "header":"",
                    "cookies":""
                }
                return self.read(face_details, exp_results)
            else:
                # Ensure that new image upload was successful -- test image details
                print("GET: New Image Details")
                face_ID = response['face_ID']
                aws_face_ID = response['aws_face_ID']
                exp_results['face_ID'] = face_ID
                exp_results['image']['aws_face_ID'] = aws_face_ID
                exp_results['image']['image_url'] = response['image_url']
                face_details = {
                    "url":data['url'] + aws_face_ID + '/',
                    "header":"",
                    "cookies":""
                }
                print(face_details['url'])
                return self.read(face_details, exp_results)


    def update(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call PUT request and verify that the status code returned is as expected
        response = PUTrequest(sURL, data['header'], data['cookies'], data['body'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            assert response['success'] == exp_results['success'], "Failed to update face details"
            self.results.update({'update_response':response['success'], 'check_update_success':'Pass'})
        except AssertionError as e:
            self.results.update({'check_update':'Fail'})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Ensure that user update was successful -- test user details
            print("GET: Updated Face Details")
            face_ID = response['face_ID']
            face_details = {
                "url":data['url'],
                "header":"",
                "cookies":""
            }
            return self.read(face_details, exp_results)



    def delete(self, data, exp_results):
        sURL = self.sHost + data['url']
        data['cookies'] = self.access_token
        print('url: ' + sURL)
        # Call PUT request and verify that the status code returned is as expected
        response = DELETErequest(sURL, data['header'], data['cookies'])
        if not verifyStatusCode(response, exp_results):
            self.results.update({'status_code':response.status_code,'check_status_code':'Fail'})
            return False, self.results
        else:
            self.results.update({'status_code':response.status_code,'check_status_code':'Pass'})
        
        # This try/catch block checks if the response data matches the expected results 
        try:
            response = response.json()
            print(response)
            if('success' in response):
                assert response['success'] == exp_results['success'], "Failed to delete"
                self.results.update({'delete_response':response['success'], 'check_delete_success':'Pass'})
            if('error' in response):
                assert response['error'].lower() == exp_results['error'].lower(), "Incorrect error message: Expecting " + exp_results['error'] + ' but got ' + response['error'] + '\n'
                self.results.update({'delete_error_response':response['error'], 'check_error':'Pass'})
                print("Successfully testd -- delete failed (face_ID does not exist)\n")
                return True, self.results
        except AssertionError as e:
            self.results.update({'check_update':'Fail'})
            print(e)
            return False, self.results
        else:
            # print('Successfully passed assertions')
            # Ensure that face deletion was successful -- test face details
            print("GET: Deleted Face Details")
            face_ID = response['face_ID']
            face_details = {
                "url":data['url'],
                "header":"",
                "cookies":""
            }
            if('response_exp_results' in exp_results):
                return self.read(face_details, exp_results['response_exp_results'])
            return False, self.results

        

