# Below are annotations for each url route.
urlpatterns = [
    url('admin/', admin.site.urls),
    # No testing needed. This url is for accessing the Django Admin Panel.

    url('login/', login_view),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/login/
    2 methods, GET and POST. They use the same URL.
    GET: does not require any parameters or body. Returns HTML for the login page.

    POST: For logging in. Requires a body with key:[value] pairs below:
    'uname': [string with username for login]
    'pword': [string with password for login]
    '''

    url(r'^(.*?)/login/?', deployment_login_view),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/QA_SSO/login/
    1 method, GET. This route serves the login HTML for single sign on. For example, a certain deployment, like TESLA, might have a custom sign on page through Microsoft Azure AD.
    GET: Requires 1 parameter, a valid KSOC_url. This KSOC_url must match a deploymentData KSOC_url. Returns HTML for the login page.
    '''

    url(r'logout/', logout_view),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/logout/
    Method does not matter. No parameters or body required. Deletes tokens from cookies of web browser.
    '''

    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    # Does not need testing. This is used for generating tokens.

    url(r'^accounts/password_reset/$', password_reset_view),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/accounts/password_reset/
    1 method, GET. Requires that you are currently logged in and sends email to current account for password reset.
    '''
    
    url(r'^accounts/password_reset/done/$', password_reset_done_view, name='password_reset_done'),
    # Does not need testing. This route is visited through clicking the email from a password reset

    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', password_reset_confirm_view, name='password_reset_confirm'),
    # Does not need testing. This route is visited through clicking the email from a password reset

    url(r'^reset/done/$', password_reset_complete_view, name='password_reset_complete'),
    # Does not need testing. This route is visited through clicking the email from a password reset.

    # ALPR
    # url(),

    # AOD
    # url(),
    
    # ASD
    # url(),

    # Chargepad Controller
    url(r'^deployments/([^/]*?)/chargepad/?$', chargepad_show),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/1/login/
    1 method, GET. Returns JSON related to chargepad for a particular deployment. 
    GET: Requires 1 parameter, a deployment_ID. 
    '''

    # Deployment Controller
    url(r'^deployments/([^/]*?)/?$', deployment_show),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/
    1 method, GET. Returns all the details of a particular deployment. 
    GET: Requires 1 parameter, a deployment_ID. 
    '''

    url(r'^deployments/([^/]*?)/alive/?$', deployment_machine_alive),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/1/alive/
    1 method, GET. Returns JSON showing which machines at the deployment_ID are online. 
    GET: Requires 1 parameter, a deployment_ID. 
    '''

    url(r'^deployments/([^/]*?)/indoormap/boundaries/?$', deployment_indoormapbounds),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/indoormap/boundaries/
    1 method, GET. Returns two coordinates for google maps to ovelay the indoor map. 
    GET: Requires 1 parameter, a deployment_ID. 
    '''

    url(r'^deployments/([^/]*?)/indoormap/get/?$', deployment_indoormap_get),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/1/indoormap/get/
    1 method, GET. Returns an AWS S3 link to a map. See confluence page for more information. https://knightscope.atlassian.net/wiki/spaces/KD/pages/649953310/How+to+add+Indoor+Map+to+KSOC
    GET: Requires 1 parameter, a deployment_ID. 
    '''
    
    url(r'^deployments/([^/]*?)/levels/?$', deployment_levels),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/1/levels/
    1 method, GET. Returns the list of levels in the deployment
    GET: Requires 1 parameter, a deployment_ID. 
    '''

    # url(r'^deployments/([^/]*?)/machinehistory/?$', deployment_machine_history), # TODO

    # Event Controller
    url(r'^deployments/([^/]*?)/eventgeneratingschedule/?$', eventController.EventGeneratingSchedule.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/eventgeneratingschedule
    2 methods, GET and POST. They use the same URL, with a deployment_id (1 in example)
    GET: Returns list of every location event boundary for that sector as JSON.

    POST: Overwrites existing schedule for given day. Requires body below:
    'event_type': [String from the following choices: 'aod', 'alpr', 'asd', 'thermal']
    'day_of_week': [String from the following choices: 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ]
    'sector_ID': [String of sector_id, example: '1', '2', '3']
    'schedule': [A string representing a list of schedules to over-write for a specific day. day_of_week for each schedule must match day_of_week above. Example below]

                '
                [
                    {
                        "start_time": 00:00:00,
                        "end_time": 01:01:01,
                        "day_of_week": "wednesday",
                        "sector_ID": "1",
                    },
                    {
                        "start_time": "07:07:07",
                        "end_time": "08:08:08",
                        "day_of_week": "wednesday",
                        "sector_ID": "1",
                    },
                ]
                '

    '''

    url(r'^deployments/([^/]*?)/eventgeneratingschedule/delete_schedule/post/?$', eventController.event_schedule_delete_post),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/eventgeneratingschedule/delete_schedule/post
    1 method, POST. 1 parameter 1 deployment_ID (1 in example)

    POST: Deletes all eventGenereatingSchedule and LocationEventData documents that match the post body criteria, shown below:
    'detection_type': [String from the following choices: 'aod', 'alpr', 'asd', 'thermal']
    'day_of_week': [String from the following choices: 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ]
    'sector_ID': [String of sector_id, example: '1']
    'deleted_locations_IDs': [A string-list of location_ids to delete in the background], example '[1, 3]'
    '''

    url(r'^deployments/([^/]*?)/events/?$', eventController.EventIndexV2.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/events
    3 methods, GET, PUT and POST. They use the same URL, with a deployment_id (1 in example)

    GET: Returns a list of events for a deployment and accepts multiple filters as parameters. All parameters are optional. Possible parameters are shown below:
   
    GET PARAMETERS:
    'page': [integer representing page of results to grab] ex. '1'
    'offset': [integer representing offset from page] ex. '0'
    'event_type': [array of detection types] ex. ['aod', 'alpr', 'asd', 'thermal']
    'sectors': [String of sector_IDs] ex. '1, 3, 4'
    'machines': [String of machine_IDs] ex. '12, 78, 36'
    'clear': [Boolean] ex. 'true'
    'start_time': [String of POSIX Timestamp, number only.] ex. '1498651200'
    'end_time': [String of POSIX Timestamp, number only.] ex. '1498652200'


    POST: Adds a new event to KSOC. Requires body below:
    
    'detection_type': [String from the following choices: 'aod', 'alpr', 'asd', 'thermal'], ex. 'alpr'
    'MIN': [String with machine_ID to log event] ex. '12'
    'time_stamp': [String of POSIX Timestamp, number only.] ex. "1600118821"

    PUT: Update the events and marks them cleared. Requires body below
    
    'comment': [String with comment], ex. 'false positive, ignore this'
    'clear': [String with True or False] ex. 'true'
    'clear_time_stamp': [String of POSIX Timestamp, number only.] ex. "1600118821"
    'event_IDs': [String of event_ID values separated by commas] ex. "1, 3, 12"
    '''


    url(r'^deployments/([^/]*?)/events/([^/]*?)/([^/]*?)/report/?$', eventController.event_report),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/1/events/1600118821/1600119821
    1 method, GET. 3 parameters, deployment_ID (1 in example), start_datetime (1600118821 in example) and end_datetime (1600119821 in example)
                   start_datetime and end_datetime are in POSIX timestamp format

    GET: Returns a CSV with a report on all detection events between the timeframe for a specific deployment
    '''

    url(r'^deployments/([^/]*?)/events/([0-9]+?)/?$',eventController.EventShowV2.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/events/4
    2 methods, GET and PUT. They use the same URL, with a deployment_ID (1 in example) and event_ID (4 in example)

    GET: Returns an event with a particular event ID. No additional body or parameters required.

    PUT: Updates the event and marks them cleared. Requires body below:
    
    'comment': [String with comment], ex. 'false positive, ignore this'
    'clear': [String with True or False] ex. 'true'
    'clear_time_stamp': [String of POSIX Timestamp, number only.] ex. "1600118821"
    'event_IDs': [String of event_ID values separated by commas] ex. "1, 3, 12"
    'false_positive': [A String with any contents], ex. "true"
    ***NOTE: the 'false_positive' body is optional. The backend expects either 'clear' or 'false_positive'
    '''

    url(r'^deployments/([^/]*?)/events/aggregate/?$', eventController.events_aggregate),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/events/aggregate
    1 method, GET. They use the same URL, with a deployment_ID (1 in example)

    GET: Returns an event with a particular event ID. No additional body or parameters required.
    '''

    url(r'^deployments/([^/]*?)/events/schedule/?$', eventController.EventScheduleV2.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/events/schedule
    2 methods, GET and POST. They use the same URL, with a deployment_ID (1 in example)

    GET: Returns all eventGeneratingSchedule documents matching criteria in the following params:
    GET PARAMETERS:
    'day_of_week': [String from the following choices: 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ], ex. 'monday'
    'detection_type': [String from the following choices: 'aod', 'alpr', 'asd', 'thermal']

    POST: Saves a schedule for each day in day_of_week for the specified deployment. Requires post body below:
    "detection_type": <string that matches "aod"/"alpr"/"thermal"/"asd">,
    "start_time": <time in format HH:MM:SS>,
    "end_time": <time in format HH:MM:SS>,
    "day_of_week": <array of days to apply the schedule ["monday", "tuesday", ...]>,
    "replace_all": <bool True=replace existing schedule for the given day_of_week, False=do not replace> (optional)

    '''

    url(r'^deployments/([^/]*?)/locationeventboundary/?$', eventController.LocationEventBoundaryIndex.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/locationeventboundary/
    2 methods, GET and POST. They use the same URL, with a deployment_ID (1 in example)

    GET: Returns list of every location event boundary for that sector

    POST: Creates a new location event boundary for a given sector. Requires the parameters below:
    "sector_ID": [String with sector_ID], ex. '1'
    "level": [Optional string with level of sector], ex. '2'
    "detection_type": [String from the following choices: 'aod', 'alpr', 'asd', 'thermal'], ex. 'aod'
    "location_name": [String with a location], ex. 'parking lot entrance'
    "metadata": [Optional string with any meta information], ex. 'metadata'
    "bounding_box_json": <-- Elliot is not sure about this one, we'll need to talk to Phil about this when you reach this.

    '''


    url(r'^deployments/([^/]*?)/locationeventboundary/([^/]*?)/?$', eventController.LocationEventBoundaryShow.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/locationeventboundary/3
    2 methods, GET and POST. They use the same URL, with a deployment_ID (1 in example) and a location_ID (3 in example)

    GET: Returns JSON list of every location event boundary for that sector given a location_ID

    PUT: Creates a new location event boundary for a given sector. Requires the parameters below:
    "detection_type": [String from the following choices: 'aod', 'alpr', 'asd', 'thermal'], ex. 'aod'
    "location_name": [String with a location], ex. 'parking lot entrance'
    "metadata": [Optional string with any meta information], ex. 'metadata'
    "bounding_box_json": <-- Elliot is not sure about this one, we'll need to talk to Phil about this when you reach this.

    '''
    # RabbitMQ User Controller
    # url(r'^auth/rmq/user/?$', rmqUserController.rmq_user_auth),
    # url(r'^auth/rmq/vhost/?$', rmqUserController.rmq_vhost_auth),

    # Sector Controller
    url(r'^deployments/([^/]*?)/sectors/?$', sector_index),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/1/sectors/
    1 method, GET. Returns the list of sectors in the deployment.
    GET: Requires 1 parameter, a deployment_ID. 
    '''

    url(r'^deployments/([^/]*?)/sectors/([^/]*?)/?$', sector_show),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/1/sectors/2/
    1 method, GET. Returns information on a single sector_id
    GET: Requires 2 parameters, a deployment_ID (1 in the example) and a sector_ID (2 in the example). 
    '''

    url(r'^deployments/([^/]*?)/sectors/([^/]*?)/boundaries/?$', boundary_index),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/1/sectors/2/boundaries/
    1 method, GET. Returns the boundaries for a specific sector in a deployment
    GET: Requires 2 parameters, a deployment_ID (1 in the example) and a sector_ID (2 in the example). 
    '''

    url(r'^deployments/([^/]*?)/sectors/([^/]*?)/boundaries/post/?$', update_boundary),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/1/sectors/2/boundaries/post
    1 method, POST. Updates the boundaries for a specific sector in a deployment.
    POST: Requires 2 parameters, a deployment_ID (1 in the example) and a sector_ID (2 in the example).
    Requires a body with key:[value] pairs below:
    'boundary': [An array of 3 coordinates]

    POST: Resets boundary points for a particular sector. A minimum of 3 boundary points must be added. Requires the values below:
    'boundary': [Array of coordinates, need at least 3. See Example below]
    EXAMPLE
    'boundary': [
        {
            latitude: 37.33,
            longitude: 121.88
        },
        {
            latitude: 37.32,
            longitude: 121.84
        },
        {
            latitude: 37.31,
            longitude: 121.80
        },
    ]
    '''

    url(r'^deployments/([^/]*?)/sector/([^/]*?)/boundaries/?$', BoundaryController.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/sector/2/boundaries
    2 methods, GET and POST. They use the same URL and have two parameters, the deployment_ID (1 in example) and sector_ID (2 in example).

    GET: Returns JSON with all boundary points for the given sector_ID

    *The post request is identical to the update_boundary controller above.
    POST: Resets boundary points for a particular sector. A minimum of 3 boundary points must be added. Requires the values below:
    'boundary': [Array of coordinates, need at least 3. See Example below]
    EXAMPLE
    'boundary': [
        {
            latitude: 37.33,
            longitude: 121.88
        },
        {
            latitude: 37.32,
            longitude: 121.84
        },
        {
            latitude: 37.31,
            longitude: 121.80
        },
    ]
    '''

    url(r'^deployments/([^/]*?)/sectors/([^/]*?)/volume/?$', volumeControllerV2.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/1/sectors/2/volume/
    1 method, PUT. 2 parameters, deployment_ID (1 in example) and sector_ID (2 in example)

    PUT: Sets volume levels for all machines in a particular sector. Requires the values below:
    'patrol_sound': [Integer between 1-100 as string]
    'alerts': [Integer between 1-100 as string]
    'intercom_concierge': [Integer between 1-100 as string]
    EXAMPLE:
    'patrol_sound': '90'
    'alerts': '80'
    'intercom_concierge': '45'
    '''

    # User Controller
    url(r'^deployments/([^/]*?)/users/?$', userController.UserIndexV2.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/users
    2 methods, GET and POST. They use the same URL and have one parameter, the deployment_id of the current user.
    GET: Returns all users that share deployments with current user, does not list admins.

    POST: Creates a new user if the current user is an admin. Requires the values below:
    'email': [email for account activation.]
    'username': [enter the same value as email here. No custom usernames allowed when creating users through this route, only emails.]
    'first_name': [string]
    'last_name': [string]
    'deployment_list': [string containing comma separated values of deployments this person can access. A good default is '1' if your current user's default deployment_id is 1]
    EXAMPLE BODY:
    'email': 'elliotsyoung@gmail.com'
    'username': 'elliotsyoung@gmail.com'
    'first_name': 'Elliot'
    'last_name': 'Young'
    'deployment_list': '1,2'
    '''

    url(r'^deployments/([^/]*?)/users/([^/]*?)/?$', userController.UserShowV2.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/deployments/1/users/ask
    2 methods, GET and PUT. They use the same URL and have two parameters: The deployment_ID (1 in example) and username (ask in example)
    GET: Returns JSON data describing the username

    PUT: Modifies an existing user with the given request body. There are several "operations" and each operation is distinguished by the 'op' key. All operation types and their data are listed below.
    
    'op': 'user_active'
    'is_active': [String of true or false] ex. 'True'
    
    'op': 'accept_terms'
    'terms_and_conditions_accepted': [String of True or False] ex. 'True
    'time_zone': [String of time zone] ex. "PDT"
    'time_stamp': [String of time stamp] ex. "2020-05-06T05:32:06Z"
    
    'op': 'update_notifications'
    'email': [String of True or False] ex. 'True'
    'voice': [String of True or False] ex. 'True'
    'text': [String of True or False] ex. 'True'
    'phone': [String of True or False] ex. 'True'
    'time_stamp': [String of time stamp] ex. "2020-05-06T05:32:06Z"
    
    'op': 'change_password'
    'old_pw': [String of old password] ex. 'pokemon'
    'new_pw': [String of new password] ex. 'digimon'
    'new_pw_confirm': [String of new password, must match] ex. 'digimon'
    
    'op': 'reset_password'
    * No request body is required for reset_password *
    
    'op': 'depeloyment_list'
    'deployment_list': [String of comma separated values for deployments the user will have access to] ex. '1,2,3'
    
    'op': 'set_permissions'
    'permissions': [String containing JSON object with permissions, see example below for all permission types]
    "{
        "permission" : {
            "can_view_machine_health" : true,
            "can_add_license_plate" : true,
            "can_view_detection_videos" : true,
            "can_view_device_statistics" : true,
            "can_make_custom_broadcast" : true,
            "can_view_list_preferences" : true,
            "alarm_sound" : true,
            "can_make_alarm_sound" : true,
            "can_view_hd_quality_video" : true,
            "can_view_investigate_page" : true,
            "can_generate_reports" : true,
            "can_download_video" : true,
            "can_edit_patrol_schedule" : true,
            "can_view_lte_usage" : true,
            "can_view_about_page" : true,
            "can_add_events" : true,
            "can_view_live_video" : true,
            "can_download_detection_videos" : true,
            "can_make_video_clip" : true,
            "can_view_device_top_ten" : true,
            "can_make_pre_recorded_messages" : true,
            "can_patrol_stop_pause_machine" : true,
            "can_view_parking_meter" : true,
            "can_view_plate_parking_meter" : true,
            "can_view_recorded_video" : true,
            "can_initiate_live_audio" : true,
            "can_view_cleared_events" : true,
            "can_clear_events" : true,
            "can_search_detection" : true,
            "can_view_machine_usage" : true,
            "can_view_timeline" : true,
            "can_edit_patrol_sound" : true,
            "admin" : false,
            "can_view_number_of_detections" : true,
            "can_view_multiple_camera_feed" : true,
            "can_add_mac_addresses" : true,
            "can_view_video_clip_history" : true,
            "can_view_control_panel" : true,
            "can_view_detection_screen" : true,
            "can_view_patrol_schedule" : true,
            "can_view_parking_utilization" : true,
            "can_initiate_intercom_call" : true
        }
    }"


    '''

    url(r'^k5/([0-9]+)/auth/?$', userController.machine_auth),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/k5/12/auth/
    1 method, GET. Returns an HTTP code of success (200) if the logged in user has access to the MIN# specifified as a parameter
    GET: Requires 1 parameter, a MIN# (12 in the example). 
    '''

    # url(r'^k5/([0-9]+)/time/([0-9]+)/auth/?$', userController.time_auth),
    url(r'^user/?$', userController.userRootV2),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/user/
    1 method, GET. Returns settings and permission set for current user 
    GET: Requires 0 parameters.
    '''

    url(r'^user/auth/?$', userController.user_auth),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/user/auth/
    1 method, GET. Returns an HTTP code of 200 of the user is authenticated. 
    GET: Requires 0 parameters.
    '''

    url(r'^user/deployments/([^/]*?)/user_deployments/?$', userController.user_alldeployments_show),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/user/deployments/1/user_deployments
    1 method, GET. Returns all deployments that a user has access to. 
    GET: Requires 1 parameter, a deployment_ID (1 in the example) that matches the user's primary deployment_ID. 
    '''

    # url(r'^user/post/?$', userController.user_ios_app_post),
    url(r'^user/user_exists/([^/]*?)/?$', userController.user_exists),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/user/user_exists/elliotyoung
    1 method, GET. Returns JSON of TRUE/FALSE if the username in parameter exists.
    GET: Requires 1 parameter, a username (elliotyoung in the example). 
    '''


    ############
    # Face Recognition Controller
    ############
    url(r'^deployments/([^/]*?)/face/?$', faceRecognitionController.FaceIndexV2.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal//ajax/api/deployments/1/face
    2 methods, GET and POST. They use the same URL, with a deployment_ID (1 in example)
    GET: Returns a list of faces for a deployment
    OPTIONAL GET PARAMETERS:
    'page': [Integer] ex. 1
    'listing': [Choice between 'BLACKLIST' and 'WHITELIST'] ex. 'BLACKLIST'

    POST: Adds a new face to the deployment
    REQUIRED POST BODY:
    'listing': [Must always be string with value 'BLACKLIST'] ex. 'BLACKLIST'
    'listed_reason': [String, any length]
    'name': [String, any length]
    '''
    ############
    url(r'^deployments/([^/]*?)/face/([^/]*?)/?$', faceRecognitionController.FaceShowV2.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/ajax/api/deployments/1/face/5f21b3f7ca00aa4385b87db2/
    3 methods, GET, PUT and DELETE. They use the same URL, with a deployment_ID (1 in example) and face_ID (5f21b3f7ca00aa4385b87db2 in example)
    GET: Returns the details a face in a deployment
    NO GET PARAMETERS

    PUT: Edit the details of a face or delete images associated with the face
    REQUIRED PUT BODY:
    There are two possible PUT bodies:
    'op': 'delete_images'  <--- Deletes all aws images associated with a face. Does not delete 
                                face document in ksoc db. Does not require any more data.

    'op': 'edit'           <--- edits an existing face. Requires parameters shown below
    'listing': [Must always be string with value 'BLACKLIST'] ex. 'BLACKLIST'
    'listed_reason': [String, any length]
    'name': [String, any length]

    DELETE: Deletes the face record in ksoc, along with all aws images saved.
    NO DELETE PARAMETERS

    '''
    ############
    url(r'^deployments/([^/]*?)/face/([^/]*?)/images/?$', faceRecognitionController.FaceImageIndexV2.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/ajax/api/deployments/1/face/5f21b3f7ca00aa4385b87db2/images
    2 methods, GET and POST. They use the same URL, with a deployment_ID (1 in example) and face_ID (5f21b3f7ca00aa4385b87db2 in example)
    GET: Returns a list of aws images for a face
    NO GET PARAMETERS

    POST: Uploads a new image to aws rekognition to associate with a face record. If no face is detected in the
          uploaded image, it will return an error.
    REQUIRED POST BODY:
    'image': [String of base64 encoded image] ex. see Postman file for example data.
    '''
    ############
    url(r'^deployments/([^/]*?)/face/([^/]*?)/images/([^/]*?)/?$', faceRecognitionController.FaceImageShowV2.as_view()),
    '''
    Example URL: https://ksoc-poc-02.knightscope.internal/ajax/api/deployments/1/face/5f21b3f7ca00aa4385b87db2/images/49011e70-a73a-4791-9025-c4f21172b85d
    2 methods, GET and DELETE. They use the same URL, with a deployment_ID (1 in example), 
    face_ID (5f21b3f7ca00aa4385b87db2 in example) and aws_face_ID (49011e70-a73a-4791-9025-c4f21172b85d in example)
    GET: Returns the details of an aws image associated to a face
    NO GET PARAMETERS

    DELETE: Delete one aws image associated with the face
    NO DELETE PARAMETERS
    '''
    ############ END OF Face Recognition CONTROLLER
]
